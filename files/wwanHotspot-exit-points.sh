#!/bin/sh

#  wwanHotspot
#
#  Wireless WAN Hotspot management application for OpenWrt routers.
#  $Revision: 3.38 $
#
#  Copyright (C) 2017-2025 Jordi Pujol <jordipujolp AT gmail DOT com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#************************************************************************

EP_users="jpujol@pcjordi live@pcjordi jpujol@gmktec"
EP_hostIPs="/var/log/${NAME}.hostIPs"

# We can forbide all communications or only the requests to servers
EP_iptables="iptables -4 --wait --table filter"
# EP_rule="INPUT -i br-lan -p all -s"
EP_rule="INPUT -i br-lan -p udp --match multiport --dports domain,ntp -s"
EP_rulelog="-m limit --limit 1/min -j LOG --log-prefix=\"[Hotspot-loop]\" --log-level info"

EP_HostNameIP() {
	local host="${1}" \
		ipv6="${2:-}" \
		ips ip4 ip6
	if [ -s "${EP_hostIPs}" ]; then
		! awk -v host="${host}" \
		-v ipv6="${ipv6}" \
		'$1 == host {if (ipv6 && $3)
			printf "%s\t%s\n", $2, $3
		else
			printf "%s\n", $2
		rc=-1
		exit}
		END{exit rc+1}' \
		< "${EP_hostIPs}" || \
			return ${OK}
	else
		: > "${EP_hostIPs}"
	fi
	ips="$(HostNameIP "${host}" "y")" || \
		return ${ERR}
	ip4="$(printf '%s\t\n' "${ips}" | cut -f 1 -s -d "${TAB}")"
	ip6="$(printf '%s\t\n' "${ips}" | cut -f 2 -s -d "${TAB}")"
	printf '%s\t%s\t%s\n' "${host}" "${ip4}" "${ip6}" \
		>> "${EP_hostIPs}"
	if [ -n "${ipv6}" -a -n "${ip6}" ]; then
		printf '%s\t%s\n' "${ip4}" "${ip6}"
	else
		printf '%s\n' "${ip4}"
	fi
}

# when Android connects Hotspot and Wifi at the same time
# we must prevent network loops
EP_LoopRemoveHost() {
	local host="${1}" \
		ip="${2}"
	while :; do
		${EP_iptables} --delete ${EP_rule} "${ip}" ${EP_rulelog} || :
		${EP_iptables} --delete ${EP_rule} "${ip}" -j DROP && \
			_applog "Removing Loop Prevention on \"${host}\" IP=${ip}" || \
			break
	done
}

EP_LoopRemove() {
	local host="${1:-"${WwanSsid}"}" \
		ip
	[ -n "${host}" ] && \
	ip="$(EP_HostNameIP "${host}")" || \
		return ${OK}
	EP_LoopRemoveHost "${host}" "${ip}"
}

# when Android connects Hotspot and Wifi at the same time
# we must prevent network loops.
# This procedure works when we assign the same network name to
# the hotspot and the wifi client, may be:
# 1. hotspot = the Android host name
# 2. or hotspot = name in the table of Static leases
# Advice: to make it work assign all of them the same name
EP_LoopPrevent() {
	local host="${1:-"${WwanSsid}"}" \
		ip
	[ -n "${host}" ] && \
	ip="$(EP_HostNameIP "${host}")" || \
		return ${OK}
	if ! ${EP_iptables} --check ${EP_rule} "${ip}" -j DROP; then
		${EP_iptables} --insert ${EP_rule} "${ip}" -j DROP && \
			_applog "Adding Loop Prevention on \"${host}\" IP=${ip}" || :
	fi
	[ -z "${Debug}" ] || {
		${EP_iptables} --check ${EP_rule} "${ip}" ${EP_rulelog} || \
			${EP_iptables} --insert ${EP_rule} "${ip}" ${EP_rulelog} || :
		${EP_iptables} --list INPUT | grep -swF "${host}" | \
			while read -r line; do
				[ -z "${line}" ] || \
					_applog "${line}"
			done
	}
}

EP_Cmd() {
	local msg="${@}" \
		addr user host \
		ip ips="" u d b
	for addr in ${EP_users}; do
		user="$(echo "${addr}" | cut -f 1 -s -d '@')"
		host="$(echo "${addr}" | cut -f 2 -s -d '@')"
		[ -n "${host}" ] && \
		for ip in $(EP_HostNameIP "${host}" "y"); do
			while IFS="${TAB}" read -r d b; do
				[ -n "${d}" ] || \
					continue
				if ssh -f "${user}@${ip}" \
				-o ConnectTimeout=1 -o ConnectionAttempts=1 \
				"DISPLAY=${d} DBUS_SESSION_BUS_ADDRESS=${b} \
				notify-send --app-name='${APPNAME}' -t 10000 '${msg}'"; then
					break 2
				fi
			done <<EOF
$(ssh "${user}@${ip}" \
-o ConnectTimeout=1 -o ConnectionAttempts=1 \
-o StrictHostKeyChecking=accept-new \
ps he -u "${user}" -o cmd | \
sed -nre  "/.* DBUS_SESSION_BUS_ADDRESS=([^[:blank:],]+) .* \
DISPLAY=(:[[:digit:]]+) .*/s//\2\t\1/p" | \
sort -u)
EOF
# use this option only in trusted local networks
# -o StrictHostKeyChecking=accept-new \
		done
	done
}

OnConnect() {
	EP_Cmd "${1}"
	EP_LoopPrevent
}

OnConnecting() {
	EP_Cmd "${1}"
}

OnDisconnect() {
	EP_Cmd "${1}"
	EP_LoopRemove
}

OnDisconnectIdle() {
	EP_Cmd "${1}"
	EP_LoopRemove
}

OnConnectFail() {
	EP_Cmd "${1}"
}

OnConnectBlacklist() {
	EP_Cmd "${1}"
}

OnNetworkFail() {
	EP_Cmd "${1}"
}

OnNetworkOK() {
	EP_Cmd "${1}"
}

OnNetworkFailMaxWarn() {
	EP_Cmd "${1}"
}

OnNetworkBlacklist() {
	EP_Cmd "${1}"
}

:
