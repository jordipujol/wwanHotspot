#!/bin/sh

#  wwanHotspot
#
#  Wireless WAN Hotspot management application for OpenWrt routers.
#  $Revision: 3.38 $
#
#  Copyright (C) 2017-2025 Jordi Pujol <jordipujolp AT gmail DOT com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#************************************************************************

_UTCseconds() {
	date +'%s'
}

_datetime() {
	date +'%F %T'
}

_ps_children() {
	local ppid=${1:-${$}} \
		excl="${2:-"0"}" \
		pid
	for pid in $(pgrep -P ${ppid} | \
	grep -svwEe "${excl}"); do
		_ps_children ${pid} "${excl}"
		pidsChildren="${pidsChildren}${pid}${TAB}"
	done
}

_ppid() {
	local pid="${1}"
	awk '$1 == "PPid:" {ppid=$2; exit}
		END{print ppid+0}' "/proc/${pid}/status" 2> /dev/null || \
	echo $(ps -ho ppid "${pid}")
}

_my_pid() {
	local pidw
	sleep 5 &
	pidw="${!}"; mypid=$(_ppid "${pidw}")
	kill "${pidw}" || :
	wait "${pidw}" || :
}

# create new pipe in /tmp
# returns: fdPipe = pipe descriptor number
FdPipe() {
	local pipe mypid
	pipe="$( _my_pid
		{ mkfifo -m a=rw "$(mktemp -u | tee /proc/${mypid}/fd/3 )" ; } 3>&1)"
	fdPipe=10
	_my_pid
	while [ -e /proc/${mypid}/fd/${fdPipe} ]; do
		let fdPipe++,1
	done
	eval "exec ${fdPipe}<>${pipe}"
	rm -f "${pipe}"
}

_applog() {
	local msg="${@}"
	printf '%s\n' "$(_datetime) ${msg}" >> "${LOGFILE}"
}

# priority: info notice warn err debug
_log() {
	local msg="${@}" \
		p="daemon.${LogPrio:-"notice"}"
	LogPrio=""
	logger -t "${NAME}" -p "${p}" "${msg}"
	_applog "${p}:" "${msg}"
}

FirewallClean() {
	local n d chain rule
	for chain in INPUT OUTPUT; do
		d="y"
		until [ -z "${d}" ]; do
			d=""
			n=1
			while rule="$(iptables -4 --wait --numeric --table filter \
			--list ${chain} ${n} 2> /dev/null)" && \
			[ -n "${rule}" ]; do
				if printf '%s\n' "${rule}" | \
				grep -qsF 'prefix "[dst-unrchble] "'; then
					iptables -4 --wait --table filter --delete ${chain} ${n}
					d="y"
				else
					let "n++,1"
				fi
			done
		done
	done
}

NtpServer() {
	local rule n d ntpserver
	if [ -n "${NtpServers}" ] && \
	[ -x "${Ntpd}" ]; then
		# Delete redirection for NTP requests,
		# allowing communication with external servers.
		d="y"
		until [ -z "${d}" ]; do
			d=""
			n=1
			while rule="$(iptables -4 --wait --numeric --table nat \
			--list zone_lan_prerouting ${n} 2> /dev/null)" && \
			[ -n "${rule}" ]; do
				if printf '%s\n' "${rule}" | \
				grep -qsF "udp dpt:ntp to:${Ip}"; then
					iptables -4 --wait --table nat ---delete zone_lan_prerouting ${n}
					d="y"
				else
					let "n++,1"
				fi
			done
		done
		if [ -z "$(uci -q get system.ntp.server)" ]; then
			uci -q delete system.ntp.interface || :
			uci add_list system.ntp.interface="br-lan"
			uci -q delete system.ntp.server || :
			while read -r ntpserver; do
				uci add_list "system.ntp.server=${ntpserver}"
			done << EOF
$(printf '%s' "${NtpServers}" | \
sed -re \
"/[[:blank:]]*(('([^']+)')|(\"([^\"]+)\")|([^[:blank:]]+))[[:blank:]]*/ \
s//\3\5\6\n/g")
EOF
			#uci commit system
			"${Ntpd}" restart
		fi
	fi
}

_exit() {
	local pidsChildren n ntpserver
	trap - EXIT INT

	FirewallClean
	printf '%s\n' "${ARGV}" | grep -qse '^WNetwork=' || \
		NtpServer
	set +o xtrace
	LogPrio="warn" _log "Exit"
	[ -z "${fdPipe:-}" ] || \
		eval "exec ${fdPipe}<&-"
	pidsChildren=""; _ps_children
	[ -z "${pidsChildren}" ] || \
		kill -s TERM ${pidsChildren} 2> /dev/null || :
	wait || :
}

Main() {
	# constants
	readonly NAME LOGFILE="/var/log/${NAME}" \
		daemon="/etc/init.d/${APPNAME}" \
		APPLOGFILE="/var/log/${APPNAME}" \
		TAB=$'\t' OK=0 ERR=1
	# internal variables, daemon scope
	local Ip Ntpd
	local LogPrio txt src dst host service proto fdPipe \
		WNetwork="" LogOutput="/dev/null"

	[ -n "${ConnectAuto:-}" ] || \
		exit ${OK}

	trap '_exit' EXIT
	trap 'exit' INT

	[ ! -f "${LOGFILE}" ] || \
		mv -f "${LOGFILE}" "${LOGFILE}_$(_UTCseconds)"
	rm -f $(ls -1 "${LOGFILE}_"* 2> /dev/null | head -qn -${LogRotate})
	exec > "${LOGFILE}" 2>&1
	if [ "${Debug:-}" = "xtrace" ]; then
		set -o xtrace
		LogOutput="${LOGFILE}"
	else
		set +o xtrace
	fi

	FdPipe

	LogPrio="warn" _log "Start"

	if [ -n "${NtpServers}" ]; then
		for Ntpd in /etc/init.d/*ntpd; do
			! "${Ntpd}" enabled || \
				break
			Ntpd=""
		done
		[ -x "${Ntpd:-}" ] || {
			echo "Error: Can't find a ntp daemon" >&2
		}
	fi
	Ntpd="${Ntpd:-}"

	Ip="$(uci -q get network.lan.ipaddr)" || {
		echo "Error: Can't find the ip address of br-lan" >&2
		exit 1
	}

	FirewallClean

	if [ -z "${WNetwork:="$(printf '%s\n' "${@}" | \
	sed -ne '/^WNetwork=/{s///;p;q}')"}" ]; then
		iptables -4 --wait --table filter --insert OUTPUT \
			-p icmp --icmp-type destination-unreachable \
			-d "$(ip -4 route show dev br-lan | awk '{print $1; exit}')" \
			-m limit --limit 1/min \
			-j LOG --log-prefix='[dst-unrchble] ' --log-level info
		if [ -n "${NtpServers}" ] && \
		[ -x "${Ntpd}" ]; then
			# Redirect NTP requests to go through router
			iptables -4 --wait --numeric --table nat --list zone_lan_prerouting | \
			tail -n +3 | \
			grep -qs -F "udp dpt:ntp to:${Ip}" || \
				iptables -4 --wait --table nat --insert zone_lan_prerouting \
				-p udp -m udp --dport ntp -j DNAT --to ${Ip}
			if [ -n "$(uci -q get system.ntp.server)" ]; then
				uci -q delete system.ntp.interface || :
				uci add_list system.ntp.interface="br-lan"
				uci -q delete system.ntp.server || :
				#uci commit system
				"${Ntpd}" restart
			fi
		fi
	else
		iptables -4 --wait --table filter --insert INPUT \
			-p icmp --icmp-type network-unreachable \
			-s "${WNetwork}" \
			-m limit --limit 1/min \
			-j LOG --log-prefix='[dst-unrchble] ' --log-level info
		NtpServer
	fi

	exec logread -e '\[dst-unrchble\] ' -f >&${fdPipe} &
	while :; do
		if read -r -u ${fdPipe} txt; then
			[ -z "${Debug}" ] || \
				while read -r src dst proto dpt; do
					[ -n "${dpt}" ] || \
						break
					host="$(ip -4 -r neighbour show "${src}" | \
						awk '{print $1; exit}')" || :
					service="$(awk -v proto="${proto}" -v dpt="${dpt}" \
						'$2 == dpt "/" tolower(proto) {print $1; exit}' \
						/etc/services)"
					printf '%s Host %s%s requests address %s:%s, protocol %s\n' \
						"$(_datetime)" \
						"${host}${host:+"="}" \
						"${src}" \
						"${dst}" \
						"${service:-"${dpt}"}" \
						"${proto}" \
						>> "${APPLOGFILE}"
				done << EOF
$(printf '%s\n' "${txt}" | \
sed -nre \
"/.*\[SRC=([^[:blank:]]+).*\
DST=([^[:blank:]]+).*\
PROTO=([^[:blank:]]+).*\
DPT=([^[:blank:]]+).*/ \
s//\1 \2 \3 \4/p")
EOF
			if "${daemon}" running; then
				[ -n "${WNetwork}" ] && \
					"${daemon}" network > /dev/null 2>&1 || \
					"${daemon}" scan > /dev/null 2>&1
				sleep 1
			elif "${daemon}" enabled; then
				"${daemon}" restart > /dev/null 2>&1
				sleep 1
			fi 2>> "${LogOutput}"
		fi
	done
}

set -o errexit -o nounset -o pipefail +o noglob +o noclobber
readonly NAME="$(basename "${0}")" \
	ARGC="${#}" \
	ARGV="$(printf '%s\n' "${0}" "${@}")"
# ARGV="$(printf '%q\n' "${0}" "${@}")" # bash only

case "${1:-}" in
start)
	shift
	Main "${@}"
	;;
*)
	echo "Wrong arguments" >&2
	exit 1
	;;
esac
