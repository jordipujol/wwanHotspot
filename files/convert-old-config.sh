#!/bin/sh

#  wwanHotspot
#
#  Wireless WAN Hotspot management application for OpenWrt routers.
#  $Revision: 3.38 $
#
#  Copyright (C) 2017-2025 Jordi Pujol <jordipujolp AT gmail DOT com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#************************************************************************

_toupper() {
	printf '%s\n' "${@}" | tr '[a-z]' '[A-Z]'
}

_unquote() {
	printf '%s\n' "${@}" | \
		sed -re "s/^([\"]([^\"]*)[\"]|[']([^']*)['])$/\2\3/"
}

_datetime() {
	date +'%F %T'
}

AddHotspot() {
	uci -q add ${configuci} hotspot
	[ -z "${net_ssid}" ] || \
		uci set ${configuci}.@hotspot[${Hotspots}].ssid="${net_ssid}"
	[ -z "${net_bssid}" ] || {
		net_bssid="$(_toupper "${net_bssid}")"
		uci add_list ${configuci}.@hotspot[${Hotspots}].bssid="${net_bssid}"
		}
	[ -z "${net_description:="$(_datetime) import old config"}" ] || \
		uci set ${configuci}.@hotspot[${Hotspots}].description="${net_description}"
	uci set ${configuci}.@hotspot[${Hotspots}].encryption="${net_encrypt}"
	[ -z "${net_key:-}" ] || \
		uci set ${configuci}.@hotspot[${Hotspots}].key="${net_key}"
	[ -z "${net_key1:-}" ] || \
		uci set ${configuci}.@hotspot[${Hotspots}].key1="${net_key1}"
	[ -z "${net_key2:-}" ] || \
		uci set ${configuci}.@hotspot[${Hotspots}].key2="${net_key2}"
	[ -z "${net_key3:-}" ] || \
		uci set ${configuci}.@hotspot[${Hotspots}].key3="${net_key3}"
	[ -z "${net_key4:-}" ] || \
		uci set ${configuci}.@hotspot[${Hotspots}].key4="${net_key4}"
	[ -z "${net_hidden:-}" ] || \
		uci set ${configuci}.@hotspot[${Hotspots}].hidden="${net_hidden}"
	[ -z "${net_blacklisted:-}" ] || {
		[ "${net_blacklisted}" = "y" ] || \
			net_blacklisted="$(_toupper "${net_blacklisted}")"
		uci add_list ${configuci}.@hotspot[${Hotspots}].blacklisted="${net_blacklisted}"
		}
	[ -z "${net_check:-}" ] || \
		uci set ${configuci}.@hotspot[${Hotspots}].check="${net_check}"
	uci commit "${configuci}"
	let "Hotspots++,1"
	unset net_description net_ssid net_bssid net_encrypt \
		net_key net_key1 net_key2 net_key3 net_key4 \
		net_hidden net_blacklisted net_check
}

config=wwanHotspot
configuci=wwanHotspotUCI

if uci -q show "${config}" || \
grep -qswe '^config' "/etc/config/${config}"; then
	echo "Current config file already complies UCI standard." >&2
	echo "Nothing to do." >&2
	exit 0
fi

: > "/etc/config/${configuci}"
uci set ${configuci}.globals=${config}
uci add_list "${configuci}.globals.remark=Global config option values"
while IFS='=' read -r option value; do
	[ -n "${option}" ] || \
		continue
	case "${option}" in
	NtpServers=*)
		uci set ${configuci}.ntp=timeserver
		while read -r ntpserver; do
			uci add_list "${configuci}.ntp.server=${ntpserver}"
		done << EOF
$(printf '%s' "${value}" | \
sed -re \
"/[[:blank:]]*(('([^']+)')|(\"([^\"]+)\")|([^[:blank:]]+))[[:blank:]]*/ \
s//\3\5\6\n/g")
EOF
		;;
	*)
		uci set "${configuci}.globals.${option}=$(_unquote "${value}")"
		;;
	esac
done << EOF
$(grep -sEe '^[[:blank:]]*[[:alpha:]]+' "/etc/config/${config}" | \
grep -svEe '^(net_|AddHotspot)' | \
sort)
EOF
# recommended ntp servers
if ! uci -q get "${configuci}.ntp.server" > /dev/null; then
	uci set ${configuci}.ntp=timeserver
	uci add_list "${configuci}.ntp.remark=when automatic connection is enabled"
	uci add_list "${configuci}.ntp.remark=wwanHotspot may manage NTP server modes:"
	uci add_list "${configuci}.ntp.remark=standalone or network client"
	for ntpserver in 0.pool.ntp.org 1.pool.ntp.org \
	2.pool.ntp.org 3.pool.ntp.org; do
		uci add_list "${configuci}.ntp.server=${ntpserver}"
	done
fi
uci commit "${configuci}"
#
Hotspots=0
unset net_description net_ssid net_bssid net_encrypt \
	net_key net_key1 net_key2 net_key3 net_key4 \
	net_hidden net_blacklisted net_check
. "/etc/config/${config}" || {
	echo "Invalid old config file." >&2
	exit 1
}
if [ ${Hotspots} -eq 0 ]; then
	hotspot=0
	while let "hotspot++,1";
	eval net_ssid=\"\${net${hotspot}_ssid:-}\";
	eval net_bssid=\"\${net${hotspot}_bssid:-}\";
	eval net_encrypt=\"\${net${hotspot}_encrypt:-}\";
	[ \( -n "${net_ssid}" -o -n "${net_bssid}" \) \
	-a -n "${net_encrypt}" ]; do
		uci -q add ${configuci} hotspot
		[ -z "${net_ssid}" ] || \
			uci set ${configuci}.@hotspot[${Hotspots}].ssid="${net_ssid}"
		[ -z "${net_bssid}" ] || \
			uci set ${configuci}.@hotspot[${Hotspots}].bssid="$(_toupper "${net_bssid}")"
		eval net_description=\"\${net${hotspot}_description:-}\";
		[ -z "${net_description:="$(_datetime) import old config"}" ] || \
			uci set ${configuci}.@hotspot[${Hotspots}].description="${net_description}"
		uci set ${configuci}.@hotspot[${Hotspots}].encryption="${net_encrypt}"
		eval net_key=\"\${net${hotspot}_key:-}\";
		[ -z "${net_key:-}" ] || \
			uci set ${configuci}.@hotspot[${Hotspots}].key="${net_key}"
		eval net_key1=\"\${net${hotspot}_key1:-}\";
		[ -z "${net_key1:-}" ] || \
			uci set ${configuci}.@hotspot[${Hotspots}].key1="${net_key1}"
		eval net_key2=\"\${net${hotspot}_key2:-}\";
		[ -z "${net_key2:-}" ] || \
			uci set ${configuci}.@hotspot[${Hotspots}].key2="${net_key2}"
		eval net_key3=\"\${net${hotspot}_key3:-}\";
		[ -z "${net_key3:-}" ] || \
			uci set ${configuci}.@hotspot[${Hotspots}].key3="${net_key3}"
		eval net_key4=\"\${net${hotspot}_key4:-}\";
		[ -z "${net_key4:-}" ] || \
			uci set ${configuci}.@hotspot[${Hotspots}].key4="${net_key4}"
		eval net_hidden=\"\${net${hotspot}_hidden:-}\";
		[ -z "${net_hidden:-}" ] || \
			uci set ${configuci}.@hotspot[${Hotspots}].hidden="${net_hidden}"
		eval net_blacklisted=\"\${net${hotspot}_blacklisted:-}\";
		[ -z "${net_blacklisted:-}" ] || \
			uci set ${configuci}.@hotspot[${Hotspots}].blacklisted="${net_blacklisted}"
		eval net_check=\"\${net${hotspot}_check:-}\";
		[ -z "${net_check:-}" ] || \
			uci set ${configuci}.@hotspot[${Hotspots}].check="${net_check}"
		uci commit "${configuci}"
		let "Hotspots++,1"
	done
fi

uci show "${configuci}" || {
	echo "Error in uci config." >&2
	exit 1
}
echo "Success." >&2
echo "Please check the result." >&2
echo "Restarting the daemon." >&2
set -x
mv -vf /etc/config/${config} /etc/config/${config}.v2-$(date +%s)
mv -vf /etc/config/${configuci} /etc/config/${config}
/etc/init.d/${config} restart
