#!/bin/sh

#  wwanHotspot
#
#  Wireless WAN Hotspot management application for OpenWrt routers.
#  $Revision: 3.38 $
#
#  Copyright (C) 2017-2025 Jordi Pujol <jordipujolp AT gmail DOT com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#************************************************************************

NAME="wwanHotspot"
COMMAND="/etc/init.d/${NAME}"
#set -x

_toupper() {
	printf '%s\n' "${@}" | tr '[a-z]' '[A-Z]'
}

_isset() {
	local option="${1}" \
		status="${2:-"on"}"
	if [ ${#option} -eq 1 ]; then
		if [ "${status}" = "on" ]; then
			[ "${-#*${option}}" != "${-}" ]
		else
			[ "${-#*${option}}" = "${-}" ]
		fi
	else
		set -o | \
		awk -v option="${option}" -v status="${status}" \
		'$1 == option {rc=-($2 == status); exit}
		END{exit rc+1}'
	fi
}

_logger() {
	! _isset "x" || \
		logger -t "wwanHotspot.iface" -p "daemon.debug" $(env)
}

WwanNetData() {
	local i=-1 j m
	WIfaceAP=""
	WIfaceSTA=""
	while let "i++,1";
	m="$(uci -q get wireless.@wifi-iface[${i}].mode)"; do
		[ "${m}" = "sta" ] || \
			continue
		NIfaceSTA="$(uci -q get wireless.@wifi-iface[${i}].network)"
		WRadioDevice="$(uci -q get wireless.@wifi-iface[${i}].device)"
		WIface="wlan$(iwinfo "${WRadioDevice}" info | \
			sed -nre '/.*PHY name: phy([[:digit:]]+)$/ s//\1/p')" || {
				echo "Error: Invalid radio device ${WRadioDevice}" >&2
				return 1
				}
		WIfaceSTA=${i}
		j=-1
		while let "j++,1";
		m="$(uci -q get wireless.@wifi-iface[${j}].mode)"; do
			if [ "${m}" = "ap" ] && \
			[ "$(uci -q get wireless.@wifi-iface[${j}].device)" = "${WRadioDevice}" ]; then
				WIfaceAP=${j}
				break
			fi
		done
		return 0
	done
	echo "Error: Can't find the STA interface" >&2
	return 1
}

CurrentHotspot() {
	Hotspot="$(printf '%s' "${Ssids}" | \
		awk -v ssid="${WwanSsid}" \
		-v bssid="${WwanBssid}" \
		'BEGIN{RS="?"; FS="\t"}
		( $2 == bssid && ( $3 == ssid || ! $3 ) ) || \
		( ! $2 && $3 == ssid ) {n=$1; exit}
		END{print n+0}')"
}

! _isset "x" || \
	exec >> "/tmp/99-wwanHotspot-$(date +'%F_%X').log" 2>&1

if [ "${ACTION}" = "ifup" -a "${INTERFACE}" = "lan" ]; then
	if "${COMMAND}" running; then
		"${COMMAND}" reload
	elif "${COMMAND}" enabled; then
		"${COMMAND}" restart
	fi
	_logger
	exit 0
fi

VARS="/var/spool/${NAME}-vars.txt"
if [ -s "${VARS}" ]; then
	. "${VARS}"
else
	WwanNetData || \
		exit 1
	GatherStatistics=""
fi

if [ "${INTERFACE}" = "${NIfaceSTA}" -o "${INTERFACE}" = "wan" ]; then
	if [ "${INTERFACE}" = "${NIfaceSTA}" -a -n "${GatherStatistics:-}" ]; then
		WwanSsid="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].ssid)" || :
		WwanBssid="$(_toupper "$(uci -q get \
			wireless.@wifi-iface[${WIfaceSTA}].bssid)")" || :
		CurrentHotspot
		statistics="/sys/class/net/${WIface}/statistics/"
		{ printf '%s\t' \
			"$(date +'%s')" \
			"${ACTION}" \
			"$(cat "${statistics}rx_bytes")" \
			"$(cat "${statistics}tx_bytes")" \
			"${Hotspot}" \
			"${WwanBssid}"
		printf '%s\n' "${WwanSsid}"
		} >> "${TRAFFIFO}"
	fi
	# ACTION 	ifdown, ifup, ifup-failed, ifupdate, free, reload, iflink, create
	case "${ACTION}" in
	ifup|ifdown|ifupdate|iflink)
		if "${COMMAND}" running; then
			"${COMMAND}" network
		elif "${COMMAND}" enabled; then
			"${COMMAND}" restart
		fi
		_logger
		;;
	esac
fi
