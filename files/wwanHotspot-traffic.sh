#!/bin/sh

#  wwanHotspot
#
#  Wireless WAN Hotspot management application for OpenWrt routers.
#  $Revision: 3.38 $
#
#  Copyright (C) 2017-2025 Jordi Pujol <jordipujolp AT gmail DOT com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#************************************************************************

_UTCseconds() {
	date +'%s' "${@}"
}

_datetime() {
	date +'%F %T' "${@}"
}

_applog() {
	local msg="${@}"
	printf '%s\n' "$(_datetime) ${msg}" >> "${LOGFILE}"
}

# priority: info notice warn err debug
_log() {
	local msg="${@}" \
		p="daemon.${LogPrio:-"notice"}"
	LogPrio=""
	logger -t "${NAME}" -p "${p}" "${msg}"
	_applog "${p}:" "${msg}"
}

_ppid() {
	local pid="${1}"
	awk '$1 == "PPid:" {ppid=$2; exit}
		END{print ppid+0}' "/proc/${pid}/status" 2> /dev/null || \
	echo $(ps -ho ppid "${pid}")
}

_my_pid() {
	local pidw
	sleep 5 &
	pidw="${!}"; mypid=$(_ppid "${pidw}")
	kill "${pidw}" || :
	wait "${pidw}" || :
}

_md5sum_text() {
	printf '%s\n' "${@}" | md5sum | cut -f 1 -s -d " "
}

_lock_acquire() {
	local pid="${1}" \
		pidw
	trap ':' "${SIGNAL}"
	while (set -o noclobber;
	! echo "${pid}" > "${lockfile}" 2> /dev/null); do
		sleep 2 &
		pidw="${!}"
		kill -s 0 "$(cat "${lockfile}")" 2> /dev/null || \
			echo "WAITING" >&${trafFifo}
		wait "${pidw}" || {
			kill "${pidw}" 2> /dev/null || :
			wait "${pidw}" || :
		}
	done
	trap - "${SIGNAL}"
}

_lock_release() {
	local pid="${1}"
	rm -f "${lockfile}"
	echo "${pid}" >&${trafFifo}
}

_exit() {
	trap - EXIT INT
	[ -z "${trafFifo:-}" ] || \
		eval "exec ${trafFifo}<&-"
	wait || :
	LogPrio="warn" _applog "Exit"
}

# global hotspot bssid ssid
SplitHour() {
	local utctimeI=${1} \
		utctimeF=${2} \
		Traffic=${3} \
		utcHour tr utiF
	while [ ${Traffic} -gt 0 ]; do
		let "utcHour=$(_UTCseconds --date="$( \
			date +'%F %H:00:00' --date=@${utctimeI})")+60*60,1"
		if [ ${utctimeF} -ge ${utcHour} ]; then
			let "tr=Traffic*(utcHour-utctimeI)/(utctimeF-utctimeI),1"
			let "utiF=utcHour-1,1"
		else
			tr=${Traffic}
			utiF=${utctimeF}
		fi
		{ printf '%s\t' \
			${utctimeI} \
			${utiF} \
			"$(date +'%F_%H' --date=@${utctimeI})" \
			${tr} \
			${hotspot} \
			"${bssid}"
		printf '%s\n' "${ssid}"
		} >> "${TRNSHOR}"
		let "Traffic-=tr,1"
		utctimeI=${utcHour}
	done
}

Transactions() {
	local transaction="${1}" \
		utctime action rx tx hotspot bssid ssid \
		Rutctime="" Tutctime="" Taction="" Trx="" Ttx="" Thotspot="" \
		Tbssid="" Tssid="" utcHour tr mypid

	[ -z "${Debug}" ] || \
		LogPrio="warn" _applog "Transactions" \
			"${transaction}"

	_my_pid

	_lock_acquire "${mypid}"

	printf '%s\n' "${transaction}" >> "${TRAFFIC}"

	[ "$(printf '%s\n' "${transaction}" | \
	cut -f 2 -s -d "${TAB}")" = "ifdown" ] || {
		_lock_release "${mypid}"
		return ${OK}
	}

	while IFS="${TAB}" \
	read -r utctime action rx tx hotspot bssid ssid; do
		case "${action}" in
		ifup)
			if [ "${Thotspot}" = ${hotspot} -a \
			"${Tbssid}" = "${bssid}" -a \
			"${Tssid}" = "${ssid}" ] && \
			[ "${Taction}" = "ifdown" -a \
			${Trx} -lt ${rx} -a \
			$((utctime-Tutctime)) -lt 30 ]; then
				# datetime from last ifdown, take also rx and tx
				let "Tutctime++,1"
			else
				# consider that a new connection has been started
				Tutctime=${utctime}
				Trx=0
				Ttx=0
			fi
			Taction="${action}"
			Thotspot=${hotspot}
			Tbssid="${bssid}"
			Tssid="${ssid}"
			;;
		ifdown)
			if [ "${Taction}" = "ifup" -a \
			"${Thotspot}" = ${hotspot} -a \
			"${Tbssid}" = "${bssid}" -a \
			"${Tssid}" = "${ssid}" ]; then
				Rutctime=${utctime}
				let "Traffic=rx-Trx+tx-Ttx,1"
				if [ ${Traffic} -gt 0 ]; then
					{ printf '%s\t' \
						${Tutctime} \
						${utctime} \
						${Traffic} \
						${hotspot} \
						"${bssid}"
					printf '%s\n' "${ssid}"
					} >> "${TRNSACT}"
					SplitHour ${Tutctime} ${utctime} ${Traffic}
				fi
			else
				Thotspot=${hotspot}
				Tbssid="${bssid}"
				Tssid="${ssid}"
			fi
			Tutctime=${utctime}
			Taction="${action}"
			Trx=${rx}
			Ttx=${tx}
			;;
		*)
			echo "Warning: Unknown action \"${action}\" time: ${utctime} " >&2
			;;
		esac
	done < "${TRAFFIC}"
	[ -z "${Rutctime}" ] || \
		sed -i.bak -nre "/^${Rutctime}\t/,$ {p}" "${TRAFFIC}"
	_lock_release "${mypid}"
}

_lock_request() {
	if [ "${transaction//[^[:digit:]]}" = "${transaction}" ]; then
		echo "${transaction}"
	elif [ "${transaction}" = "WAITING" ] && \
	! kill -s 0 "$(cat "${lockfile}")" 2> /dev/null; then
		cat "${lockfile}" 2> /dev/null
	else
		return ${ERR}
	fi
}

Main() {
	# constants
	readonly NAME LOGFILE="/var/log/${NAME}" SIGNAL="USR1" \
		OK=0 ERR=1 \
		LF=$'\n' TAB=$'\t'
	# internal variables, daemon scope
	local LogPrio transaction pidsChildren \
		TRAFFIC TRNSACT TRNSHOR lockfile pidw trafFifo

	[ -n "${GatherStatistics:-}" ] || \
		exit ${OK}

	TRAFFIC="${GatherStatistics}${APPNAME}-traf.txt"
	TRNSACT="${GatherStatistics}${APPNAME}-trns.txt"
	TRNSHOR="${GatherStatistics}${APPNAME}-trhr.txt"
	trafFifo=10
	while [ -e /proc/${$}/fd/${trafFifo} ]; do
		let trafFifo++,1
	done
	eval "exec ${trafFifo}<>${TRAFFIFO}"

	trap '_exit' EXIT
	trap 'exit' INT

	[ ! -f "${LOGFILE}" ] || \
		mv -f "${LOGFILE}" "${LOGFILE}_$(_UTCseconds)"
	rm -f $(ls -1 "${LOGFILE}_"* 2> /dev/null | head -qn -${LogRotate})
	[ "${Debug:-}" != "xtrace" ] && \
		set +o xtrace || \
		set -o xtrace
	exec > "${LOGFILE}" 2>&1

	LogPrio="warn" _applog "Start"

	lockfile="/var/lock/$(_md5sum_text "${TRAFFIC}")"
	while :; do
		if read -r -u ${trafFifo} transaction; then
			if pidw="$(_lock_request)"; then
				wait ${pidw} 2> /dev/null || :
				rm -f "${lockfile}"
				pidsChildren="$(pgrep -P "${$}")" && \
					kill -s "${SIGNAL}" ${pidsChildren} 2> /dev/null || :
			else
				Transactions "${transaction}" &
			fi
		fi
	done
}

set -o errexit -o nounset -o pipefail +o noglob +o noclobber
NAME="$(basename "${0}")"
case "${1:-}" in
start)
	shift
	Main "${@}"
	;;
*)
	echo "Wrong arguments" >&2
	exit 1
	;;
esac
