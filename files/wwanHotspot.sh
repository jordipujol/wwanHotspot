#!/bin/sh

#  wwanHotspot
#
#  Wireless WAN Hotspot management application for OpenWrt routers.
#  $Revision: 3.38 $
#
#  Copyright (C) 2017-2025 Jordi Pujol <jordipujolp AT gmail DOT com>
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 3, or (at your option)
#  any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#************************************************************************

_toupper() {
	printf '%s\n' "${@}" | tr '[a-z]' '[A-Z]'
}

_unquote() {
	printf '%s\n' "${@}" | \
		sed -re "s/^([\"]([^\"]*)[\"]|[']([^']*)['])$/\2\3/"
}

_min() {
	printf '%s\n' "${@}" | sort -n | head -n 1
}

_max() {
	printf '%s\n' "${@}" | sort -n -r | head -n 1
}

_indicator() {
	local value="${1}"
	value="$(printf '%s\n' "${value}" | \
	grep -svxiEe '[[:blank:]]*|0|false|n|no|disable|disabled|off')" || \
		return ${OK}
	shift
	[ ${#} -gt ${NONE} ] && \
	printf '%s\n' "${@}" | grep -sixF "${value}" || \
		printf '%s\n' "y"
}

_RcInt() {
	kill -l "${@}" | \
		awk '{printf "%s%d", sep, $1+128; sep="\t"}'
}

_sleep() {
	local timelapse="${1}"
	while let "timelapse--"; do
		sleep 1
	done > /dev/null 2>&1
}

_list_append() {
	local l="${1}" \
		v="${2}" \
		s="${3:-"${TAB}"}"
	eval ${l}='$(eval printf '%s' \"\${${l}:-}\")${v}${s}'
}

_list_get() {
	local l="${1}" \
		i="${2}" \
		s="${3:-"${TAB}"}"
	eval printf '%s\\n' \"\${${l}:-}\" | \
		awk -v i="${i}" -v s="${s}" \
		'BEGIN{FS=s}
		0 < i && i < NF {print $i; rc=-1}
		{exit}
		END{exit rc+1}'
}

_list_remove() {
	local l="${1}" \
		i="${2}" \
		s="${3:-"${TAB}"}" \
		m r=""
	_list_get "${l}" "${i}" "${s}" > /dev/null || \
		return ${ERR}
	[ ${i} -le 1 ] || \
		r="-$((i-1)),"
	r="${r}$((i+1))-"
	m="$(eval printf '%s' \"\${${l}:-}\" | \
		cut -s -f "${r}" -d "${s}")"
	[ -n "${m}" ] && \
		eval ${l}='${m}' || \
		unset "${l}" || :
}

_UTCseconds() {
	date +'%s' "${@}"
}

_datetime() {
	date +'%F %T' "${@}"
}

_msgdatetime() {
	_datetime "--date=@${MsgTime}"
}

_ps_children() {
	local ppid=${1:-${$}} \
		excl="${2:-"0"}" \
		pid
	for pid in $(pgrep -P ${ppid} | \
	grep -svwEe "${excl}"); do
		_ps_children ${pid} "${excl}"
		pidsChildren="${pidsChildren}${pid}${TAB}"
	done
}

_applog() {
	local msg="${@}"
	printf '%s\n' "$(_datetime) ${msg}" >> "${LOGFILE}"
}

# priority: info notice warn err debug
_log() {
	local msg="${@}" \
		p="daemon.${LogPrio:-"notice"}"
	LogPrio=""
	logger -t "${NAME}" -p "${p}" "${msg}"
	_applog "${p}:" "${msg}"
}

_msg() {
	msg="${@}"
}

_check_int_val() {
	local n="${1}" \
		d="${2}" \
		v="" w rc=${OK}
	eval w=\"\${${n}:-}\"
	let "v=${w}" 2> /dev/null || \
		rc=${?}
	if [ ${rc} -gt 1 ] || [ -z "${v}" ] || [ ${v} -lt ${NONE} ]; then
		_applog "Config error:" \
			"Invalid integer value \"${w}\" for \"${n}\"," \
			"assuming default \"${d}\""
		v="${d}"
	fi
	let "${n}=${v},1"
}

# parm: list of BSSIDs
# output: uppercase of BSSIDs
# return code: OK or ERR when any BSSID is invalid
BssidsCheck() {
	local bssids="${@}"
	printf '%s\n' "${bssids}" | \
	sed -re "/[[:blank:]'\"]+/s//\t/g" \
	-e '/^[[:blank:]]+|[[:blank:]]+$/s///g' | \
	awk 'NF == 0 {rc=1; exit}
		{ for (i=1; i <= NF; i++) {
			if ( !$i )
				continue
			printf "%s\t", toupper($i)
			if ( !($i ~ "^([[:xdigit:]]{2}:){5}[[:xdigit:]]{2}$") )
				rc=1
			}
		}
		END{exit rc+0}'
}

HostNameIP() {
	local host="${1}" \
		ipv6="${2:-}" \
		nameserver staticlease
	nameserver="$(awk '$1 == "nameserver" {print $2; exit}' < /etc/resolv.conf)"
	! nslookup "${host}" "${nameserver:-"127.0.0.1"}" 2> /dev/null | \
		awk -v host="${host}" \
		-v ipv6="${ipv6}" \
		'function isIP(s, a, i) {
			if (split(s, a, ".") == 4) {
				for (i in a)
					if (a[i] !~ /^[[:digit:]]+$/ || a[i] > 255)
						return
				return sprintf("%d.%d.%d.%d", a[1], a[2], a[3], a[4])
			} else
			if (ipv6 && split(s, a, ":") == 5)
				return s
		}
		$NF ~ "^" host "." && $1 == "Address" {
			if (ip=isIP($3)) {
				if (ips)
					ips=ips "\t" ip
				else
					ips=ip
				if (! ipv6) exit
			}
		}
		$NF ~ "^" host "." && $1 == "Address:" {
			if (ip=isIP($2)) {
				if (ips)
					ips=ips "\t" ip
				else
					ips=ip
				if (! ipv6) exit
			}
		}
		END{if (ips)
				print ips
			else
				exit 1}' || \
			return ${OK}
	staticlease="$(uci show dhcp | \
		sed -nr \
		-e '/dhcp.@host\[([[:digit:]]+)\].name='"'${host}'"'/{s//\1/;p;q}')"
	[ -n "${staticlease}" ] && \
	uci -q get dhcp.@host[${staticlease}].ip || \
		return ${ERR}
}

_pids_active() {
	local p rc=${ERR}
	for p in "${@}"; do
		if kill -s 0 ${p} 2> /dev/null; then
			printf '%d\t' ${p}
			rc=${OK}
		fi
	done
	return ${rc}
}

WaitSubprocess() {
	local pids="${1}" \
		dontInterrupt="${2:-}" \
		pidw rc=""
	( _sleep ${Interval} && kill -s TERM ${pids} 2> /dev/null) &
	pidw=${!}
	while wait ${pids} && rc=${OK} || rc=${rc:-${?}};
	pids="$(_pids_active ${pids})"; do
		if [ "${dontInterrupt}" = "y" ] || \
		( [ "${#rc}" = 3 ] && ( \
		[ "${dontInterrupt}" != "${dontInterrupt//${rc}}" ] || \
		[ -n "${NoSleep}" -a \
		"${dontInterrupt}" != "${dontInterrupt//${NoSleep}}" ] ) ); then
			rc=""
		else
			kill -s TERM ${pids} 2> /dev/null || :
		fi
	done
	kill -s TERM ${pidw} 2> /dev/null || :
	wait ${pidw} || :
	return ${rc:-${OK}}
}

StatusName() {
	local status="${1:-}"
	echo -n "Actual Status "
	case ${status} in
	${NONE}) echo "NONE";;
	${DISCONNECTED}) echo "DISCONNECTED";;
	${CONNECTING}) echo "CONNECTING";;
	${DISABLED}) echo "DISABLED";;
	${CONNECTED}) echo "CONNECTED";;
	*) echo "Invalid";;
	esac
}

HotspotName() {
	local hotspot="${1:-"${Hotspot}"}" \
		bssid="${2:-"${WwanBssid:-}"}" \
		ssid="${3:-"${WwanSsid:-"${BEL}"}"}"
	[ "${bssid}" != "${BEL}" ] || \
		bssid=""
	[ "${ssid}" = "${BEL}" ] && \
		ssid="${NULLSSID}" || \
		ssid="\"${ssid}\""
	echo "${hotspot}.${bssid%${TAB}}.${ssid}"
}

ChangeStatus() {
	local toStatus=${1} \
		force="${2:-}" \
		msg=""
	[ -z "${Debug}" ] || \
		_applog "$(StatusName ${toStatus})"
	[ ${Status} -ne ${toStatus} -o -n "${force}" ] || \
		return ${OK}
	[ ${Status} -ne ${NONE} -a -z "${force}" ] || \
		msg="Status: "
	if [ ${toStatus} -eq ${CONNECTED} ]; then
		Status=${toStatus}
		[ -z "${ExitPoints}" ] || {
			[ ${NetwFailures} -eq ${NONE} -o -z "${force}" ] && \
				msg="${msg}Connected to $(HotspotName)" || \
				MsgNetwFailures
			((OnConnect "${msg}" >> "${DevNull}" 2>&1) &)
		}
		[ -z "${ConnectAuto}" ] || \
			((ExecConnectAuto "start" >> "${DevNull}" 2>&1) &)
	elif [ ${toStatus} -eq ${CONNECTING} ]; then
		msg="${msg}Connecting to $(HotspotName)"
		Status=${toStatus}
		[ -z "${ExitPoints}" ] || \
			((OnConnecting "${msg}" >> "${DevNull}" 2>&1) &)
		[ -z "${ConnectAuto}" ] || \
			((ExecConnectAuto "start" >> "${DevNull}" 2>&1) &)
	elif [ -n "${force}" ] || \
	[ ${Status} -eq ${NONE} -o ${Status} -eq ${CONNECTED} ]; then
		[ ${Status} -eq ${NONE} ] && \
			msg="${msg}Disconnected" || \
			msg="${msg}Disconnected $(HotspotName)"
		Status=${toStatus}
		[ -z "${ExitPoints}" ] || \
			((OnDisconnect "${msg}" >> "${DevNull}" 2>&1) &)
		[ -z "${ConnectAuto}" ] || \
			((ExecConnectAuto >> "${DevNull}" 2>&1) &)
	fi
	Status=${toStatus}
}

StatusHasChanged() {
	Msgs=""
	[ -n "${MsgsChgd}" ] || \
		MsgsInfo=""
	UpdateReport="y"
}

AddMsg() {
	MsgTime=$(_UTCseconds)
	local msg="$(_msgdatetime) ${@}"
	Msgs="${Msgs:+"${Msgs}${LF}"}${msg}"
	MsgsChgd="y"
}

AddMsgInfo() {
	MsgTime=$(_UTCseconds)
	local msg="$(_msgdatetime) ${@}"
	MsgsInfo="${MsgsInfo:+"${MsgsInfo}${LF}"}${msg}"
	MsgsChgd="y"
}

InsertStatMsg() {
	local msg="${@:-}"
	[ -z "${msg}" ] || {
		_log "${msg}"
		msg="${LogPrio}: ${msg}"
	}
	grep -qsF "Radio device is" "${STATFILE}" && \
		sed -i -e "/^Radio device is / i ${msg}" "${STATFILE}" || \
		printf '%s\n' "${msg}" >> "${STATFILE}"
}

MsgNetwFailures() {
	_msg "${NetwFailures} networking" \
		"failure$([ ${NetwFailures} -le 1 ] || echo "s")" \
		$([ ${BlackListNetworkMaxWarn} -ge ${NetwFailures} ] || \
		echo "since $(_datetime "--date=@${NetwFailTime}")") \
		"on $(HotspotName)"
}

_exit() {
	local pidsChildren
	trap - EXIT INT ${IRELOAD} ${INETW} ${ISCAN} ${ISTAT}
	set +o xtrace
	InsertStatMsg "Daemon exit."
	InsertStatMsg
	[ -z "${WRadioDevice:-}" ] || \
		uci set wireless.${WRadioDevice}.channel="auto"
	/etc/init.d/${NAME}-autoconnect stop > /dev/null 2>&1
	pidsChildren=""; _ps_children
	[ -z "${pidsChildren}" ] || \
		kill -s TERM ${pidsChildren} > /dev/null 2>&1 || :
	wait || :
}

IfaceTraffic() {
	local iface="${1:-"${WIface}"}" \
		statistics
	statistics="/sys/class/net/${iface}/statistics/"
	printf '%s\n' $(($(cat "${statistics}rx_bytes")+ \
		$(cat "${statistics}tx_bytes"))) 2> /dev/null
}

BlackListHotspot() {
	local cause="${1}" \
		expires="${2}" \
		reason="${3}" \
		utctime=$(_UTCseconds) \
		exp=${NONE} msg
	msg="Blacklisting $(HotspotName)"
	_list_append "hotspot${Hotspot}_blacklisted" \
		"${WwanBssid}"
	if [ ${expires} -gt ${NONE} ]; then
		let "exp=expires+utctime,1"
		[ -n "${BlackListEnds}" ] && \
		[ ${BlackListEnds} -le ${exp} ] || \
			BlackListEnds=${exp}
		msg="${msg} for ${expires} seconds"
	fi
	_list_append "hotspot${Hotspot}_blacklist" \
		"${exp}.${cause}.${utctime}"
	NetwFailures=${NONE}
	NetwFailTime=${NONE}
	StatusHasChanged
	LogPrio="warn"; _log "${msg}"
	AddMsg "${msg}"
	[ -z "${ExitPoints}" ] || \
		case "${cause}" in
		network)
			((OnNetworkBlacklist "${msg}" >> "${DevNull}" 2>&1) &)
		;;
		connect)
			((OnConnectBlacklist "${msg}" >> "${DevNull}" 2>&1) &)
		;;
		esac
	LogPrio="warn"; _log "Reason:" "${reason}"
	AddMsg "Reason:" "${reason}"
}

BlackListExp() {
	local hotspot bl
	set | \
	sed -nre "\|^hotspot([[:digit:]]+)_blacklist='(.*)'$| s||\1 \2|p" | \
	while read -r hotspot bl; do
		printf '%s' "${bl}" | \
		awk -v hotspot=${hotspot} \
			'BEGIN{RS="\t"; FS="."}
			$1 > 0 {printf "%d %d %d\n", $1, hotspot, NR}'
	done | \
	sort -n -k 1,1
}

BlackListExpired() {
	local utctime="" hotspot index bssid \
		msg rc=""

	[ -n "${BlackListEnds}" ] && \
	[ ${BlackListEnds} -le ${utctime:=$(_UTCseconds)} ] || \
		return ${OK}

	BlackListEnds=""
	while read -r BlackListEnds hotspot index && \
	[ -n "${BlackListEnds}" ] && \
	[ ${BlackListEnds} -le ${utctime} ]; do
		bssid="$(_list_get "hotspot${hotspot}_blacklisted" ${index} || :)"
		_list_remove "hotspot${hotspot}_blacklisted" ${index} || :
		_list_remove "hotspot${hotspot}_blacklist" ${index} || :
		_msg "Blacklisting has expired for" \
			"$(HotspotName "${hotspot}" \
				"${bssid:-"${BEL}"}" \
				"$(eval printf '%s' \"\${hotspot${hotspot}_ssid:-\"${BEL}\"}\")")"
		if [ -z "${rc}" ]; then
			[ ${Status} -eq ${CONNECTED} -o ${Status} -eq ${CONNECTING} ] && \
				UpdateReport="y" || \
				StatusHasChanged
			rc="y"
		fi
		_applog "${msg}"
		AddMsgInfo "${msg}"
		BlackListEnds=""
	done << EOF
$(BlackListExp)
EOF
}

WifiCarrier() {
	local iface="${1:-"${WIface}"}"
	cat "/sys/class/net/${iface}/carrier" 2>> "${DevNull}" || :
}

WatchWifi() {
	local c=${Sleep}
	while [ "$(WifiCarrier)" != "1" ] && \
	let "c--"; do
		sleep 1
	done
}

IsWanConnected() {
	local status
	[ -n "${IfaceWan}" ] && \
	status="$(cat "/sys/class/net/${IfaceWan}/operstate" 2> /dev/null)" && \
	[ -n "${status}" -a "${status}" != "down" ]
}

SetWanShutdown() {
	[ -n "${WanShutdown}" -a -n "${IfaceWan}" ] && \
	! IsWanConnected || \
		return ${OK}
	if ! ifconfig "${IfaceWan}" | grep -qswF 'RUNNING'; then
		[ -z "${Debug}" ] || \
			_applog "WAN interface is already down"
		return ${OK}
	fi
	_applog "Warn:" \
		"WAN interface is disconnected." \
		"Disabling interface ${IfaceWan}"
	ifdown wan || :
}

WwanGateway() {
	ip -4 route show default dev "${WIface}" 2> /dev/null | \
		awk 'function isIP(s, a, i) {
			if (split(s, a, ".") != 4) return
			for (i in a)
				if (a[i] !~ /^[[:digit:]]+$/ || a[i] > 255)
					return
			return sprintf("%d.%d.%d.%d", a[1], a[2], a[3], a[4])
		}
		$1 == "default" {
			if (ip=isIP($3)) {print ip; rc=-1; exit}
		}
		END{exit rc+1}'
}

WwanNetworkIP() {
	[ ${Status} -eq ${CONNECTED} ] && \
	ip -4 route show dev "${WIface}" 2> /dev/null | \
		awk '$1 ~ "^([[:digit:]]+\.){3}[[:digit:]]+[/][[:digit:]]+$" {
		print $1; rc=-1; exit}
		END{exit rc+1}'
}

WwanIfaceIP() {
	ip -4 route show dev "${WIface}" 2> /dev/null | \
		awk 'function isIP(s, a, i) {
			if (split(s, a, ".") != 4) return
			for (i in a)
				if (a[i] !~ /^[[:digit:]]+$/ || a[i] > 255)
					return
			return sprintf("%d.%d.%d.%d", a[1], a[2], a[3], a[4])
		}
		{for (i=1; i < NF; i++)
			if ($i == "src")
				if (ip=isIP($(i+1))) {print ip; rc=-1; exit}
		}
		END{exit rc+1}'
}

IsWwanDisconnected() {
	[ -n "$(WwanIfaceIP)" ] || \
		echo "y"
}

SetEncryption() {
	local encryption="${1:-}" \
		key="${2:-}" \
		key1="${3:-}" \
		key2="${4:-}" \
		key3="${5:-}" \
		key4="${6:-}"
	if [ -z "${encryption}" ]; then
		eval encryption=\"\${hotspot${Hotspot}_encryption:-}\"
		eval key=\"\${hotspot${Hotspot}_key:-}\"
		eval key1=\"\${hotspot${Hotspot}_key1:-}\"
		eval key2=\"\${hotspot${Hotspot}_key2:-}\"
		eval key3=\"\${hotspot${Hotspot}_key3:-}\"
		eval key4=\"\${hotspot${Hotspot}_key4:-}\"
	fi
	uci set wireless.@wifi-iface[${WIfaceSTA}].encryption="${encryption}"
	uci set wireless.@wifi-iface[${WIfaceSTA}].key="${key}"
	uci set wireless.@wifi-iface[${WIfaceSTA}].key1="${key1}"
	uci set wireless.@wifi-iface[${WIfaceSTA}].key2="${key2}"
	uci set wireless.@wifi-iface[${WIfaceSTA}].key3="${key3}"
	uci set wireless.@wifi-iface[${WIfaceSTA}].key4="${key4}"
}

PleaseGatherStats() {
	local action statistics sl=1
	[ -n "${GatherStatistics}" ] && \
	[ -n "$(WwanIfaceIP)" ] || \
		return ${OK}
	msg="Gathering traffic statistics"
	AddMsgInfo "${msg}"
	_applog "${msg}"
	statistics="/sys/class/net/${WIface}/statistics/"
	for action in "ifdown" "ifup"; do
		{ printf '%s\t' \
			"$(_UTCseconds)" \
			"${action}" \
			"$(cat "${statistics}rx_bytes")" \
			"$(cat "${statistics}tx_bytes")" \
			"${Hotspot}" \
			"${WwanBssid}"
		printf '%s\n' "${WwanSsid}"
		} >> "${TRAFFIFO}"
		sleep $((sl--))
	done
}

PleaseReport() {
	local msg="${@:-"Updating status report"}"
	[ -n "${UpdateReport}" ] || {
		MsgsInfo=""
		UpdateReport="y"
	}
	_applog "${msg}"
	AddMsgInfo "${msg}"
}

RequestingStatus() {
	local msg="${@}"
	[ ${Status} -ne ${CONNECTING} ] || \
		return ${OK}
	PleaseGatherStats &
	ChangeStatus ${Status} "y"
	PleaseReport "${msg}"
	NoSleep="${RcISTAT}"
	MsgsChgd="y"
}

RequestingReload() {
	[ -n "${ReloadConfig}" ] || {
		NoSleep="${RcIRELOAD}"
		ReloadConfig="y"
	}
}

RequestingNetwork() {
	NoSleep="${RcINETW}"
}

RequestComplete() {
	local msg
	[ -n "${RequestTimeout}" ] || \
		return ${OK}
	msg="${@:-"Scan request complete"}"
	_applog "${msg}"
	AddMsgInfo "${msg}"
	RequestTimeout=""
}

RequestingScan() {
	local msg
	msg="Scan request received"
	if [ -n "${RequestTimeout}" ]; then
		if [ ${RequestTimeout} -ge $(_UTCseconds) ]; then
			[ -z "${Debug}" ] || \
				_applog "${msg}." "Ignored"
			return ${OK}
		fi
		RequestComplete "Scan request timeout"
	fi
	case ${Status} in
	${CONNECTED})
		_msg "${msg}" "when a Hotspot is already connected"
		RequestTimeout=$(($(_UTCseconds)+SleepScanAuto))
		NoSleep="${RcISCAN}"
		[ -z "${ReScan}" ] || \
			IndReScan="y"
		MsgsInfo=""
		MsgsChgd="y"
		;;
	${CONNECTING})
		_msg "${msg}" "when a Hotspot is connecting. Ignored"
		;;
	*)
		RequestTimeout=$(($(_UTCseconds)+SleepScanAuto))
		NoSleep="${RcISCAN}"
		[ -n "${ScanAuto}" ] || \
			ScanRequest=1
		MsgsInfo=""
		MsgsChgd="y"
		;;
	esac
	_applog "${msg}"
	AddMsgInfo "${msg}"
}

ConnectedBssid() {
	iwinfo "${WIface}" info 2> /dev/null | \
	awk '/^[[:blank:]]+Access Point:[[:blank:]]+/ { bssid=toupper($NF) }
	/Mode: Client/ { print bssid; rc=-1; exit }
	END{exit rc+1}'
}

ConnectedSsid() {
	iwinfo "${WIface}" info 2> /dev/null | \
	awk -v iface="${WIface}" \
	'$1 == iface && $2 == "ESSID:" {
		ssid=gensub(/.*ESSID:[[:blank:]]+"|"$/, "", "g") }
	/Mode: Client/ { if (ssid) { print ssid; rc=-1 }
		exit }
	END{exit rc+1}'
}

ValidateSsidBssid() {
	local ssid bssid rc=""
	if [ -z "${WwanSsid}" ] && \
	ssid="$(ConnectedSsid)" && \
	[ -n "${ssid}" -a "${ssid}" != "${NULLSSID}" ]; then
		rc="y"
		uci set wireless.@wifi-iface[${WIfaceSTA}].ssid="${ssid}"
	fi
	if [ -z "${WwanBssid}" ] && \
	bssid="$(ConnectedBssid)"; then
		rc="y"
		uci set wireless.@wifi-iface[${WIfaceSTA}].bssid="${bssid}"
	fi
	if [ -n "${rc}" ]; then
		wifi down "${WRadioDevice}"
		wifi up "${WRadioDevice}"
		UpdateReport="y"
		return ${ERR}
	fi
}

ImportHotspot() {
	local hotspot_ssid hotspot_hidden="" hotspot_bssid dummy msg ssid

	uci -q add ${NAME} hotspot
	dummy="$(_datetime) Automatic hotspot import"
	uci set ${NAME}.@hotspot[${Hotspots}].description="${dummy}"
	hotspot_ssid="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].ssid)" || :
	if ssid="$(ConnectedSsid)"; then
		if [ "${ssid}" = "${NULLSSID}" ]; then
			hotspot_hidden="y"
		elif [ -z "${hotspot_ssid}" ]; then
			hotspot_ssid="$(_unquote "${ssid}")"
		fi
	fi
	uci set ${NAME}.@hotspot[${Hotspots}].ssid="${hotspot_ssid}"
	[ -z "${hotspot_hidden}" ] || \
		uci set ${NAME}.@hotspot[${Hotspots}].hidden="${hotspot_hidden}"
	hotspot_bssid="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].bssid)" || :
	[ -n "${hotspot_bssid}" ] || \
		hotspot_bssid="$(ConnectedBssid)" || :
	uci add_list ${NAME}.@hotspot[${Hotspots}].bssid="$(_toupper "${hotspot_bssid}")"
	! dummy="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].encryption)" || \
		uci set ${NAME}.@hotspot[${Hotspots}].encryption="${dummy}"
	! dummy="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].key)" || \
		uci set ${NAME}.@hotspot[${Hotspots}].key="${dummy}"
	! dummy="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].key1)" || \
		uci set ${NAME}.@hotspot[${Hotspots}].key1="${dummy}"
	! dummy="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].key2)" || \
		uci set ${NAME}.@hotspot[${Hotspots}].key2="${dummy}"
	! dummy="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].key3)" || \
		uci set ${NAME}.@hotspot[${Hotspots}].key3="${dummy}"
	! dummy="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].key4)" || \
		uci set ${NAME}.@hotspot[${Hotspots}].key4="${dummy}"
	uci add_list ${NAME}.@hotspot[${Hotspots}].remark='option check "https://www.google.com/"'
	uci add_list ${NAME}.@hotspot[${Hotspots}].remark='option hidden "y"'
	uci add_list ${NAME}.@hotspot[${Hotspots}].remark='list blacklisted'
	msg="Importing the current router setup for the STA interface"
	[ ${Hotspots} -ne ${NONE} ] || \
		msg="No hotspots configured, ${msg}"
	LogPrio="warn"; _log "${msg}"
	AddMsgInfo "Warn:" "${msg}"
	uci commit "${NAME}"
}

PreBackupRotate() {
	local f
	for f in "${LOGFILE}" "${LOGFILE}.xtrace"; do
		[ ! -f "${f}" ] || \
			mv -f "${f}" "${f}_${MsgTime}"
	done
}

BackupRotate() {
	local f t
	find "$(dirname "${LOGFILE}")" -maxdepth 1 \
	-name "$(basename "${LOGFILE}")_*" | \
	sort | \
	head -qn -${LogRotate} | \
	while IFS="_" read -r f t; do
		rm -f "${LOGFILE}_${t}" "${LOGFILE}.xtrace_${t}"
	done
}

WwanNetData() {
	local i=-1 j m phy
	WRadioDevice=""
	WIface=""
	WIfaceAP=""
	WIfaceSTA=""
	NIfaceAP=""
	NIfaceSTA=""
	while let "i++,1";
	m="$(uci -q get wireless.@wifi-iface[${i}].mode)"; do
		[ "${m}" = "sta" ] || \
			continue
		WRadioDevice="$(uci -q get wireless.@wifi-iface[${i}].device)"
		phy="$(iwinfo "${WRadioDevice}" info | \
		awk '/PHY name:/ {print $NF; rc=-1; exit}
		END{exit rc+1}')" && \
			WIface="$(iwinfo | \
			awk -v phy="${phy}" \
			'$2 == "ESSID:" {dev=$1}
			/PHY name:/{if ($NF == phy) {print dev; rc=-1; exit} }
			END{exit rc+1}' || \
			echo "wlan${phy:$((${#phy}-1))}")" || {
				LogPrio="err"; InsertStatMsg "Invalid radio device ${WRadioDevice}"
				exit ${ERR}
			}
		LogPrio="info"; _log "Found STA config in wifi-iface ${i}"
		WIfaceSTA=${i}
		NIfaceSTA="$(uci -q get wireless.@wifi-iface[${i}].network)"
		j=-1
		while let "j++,1";
		m="$(uci -q get wireless.@wifi-iface[${j}].mode)"; do
			if [ "${m}" = "ap" ] && \
			[ "$(uci -q get wireless.@wifi-iface[${j}].device)" = "${WRadioDevice}" ]; then
				LogPrio="info"; _log "Found AP config in wifi-iface ${j}"
				WIfaceAP=${j}
				NIfaceAP="$(uci -q get wireless.@wifi-iface[${j}].network)"
				break
			fi
		done
		return ${OK}
	done
	return ${ERR}
}

ActiveDefaultRoutes() {
	ip -4 route show default | \
	awk '$1 == "default" && $NF != "linkdown" {n++}
		END{print n+0}'
}

LoadConfig() {
	local hotspot_ssid hotspot_bssid hotspot_encryption dummy option value \
		hotspot bssids bssid d \
		msg="Loading configuration"

	ReloadConfig="y"
	# config variables, default values
	Debug=""
	WanShutdown=""
	APDisabled=""
	APChannel="auto"
	WAnalyzer=""
	ScanAuto="y"
	ReScan="y"
	ReScanOnNetwFail=1
	ReScanAuto=${NONE}
	ConnectAuto=""
	NtpServers=""
	Sleep=20
	SleepInactive=$((Sleep*3))
	SleepDsc=$((Sleep*3))
	SleepScanAuto=$((Sleep*15))
	BlackList=3
	BlackListExpires=${NONE}
	BlackListNetwork=3
	BlackListNetworkMaxWarn=3
	BlackListNetworkExpires=$((10*60))
	PingWait=7
	MinTrafficBps=2048
	ShowTraffic=""
	DisconnectIdle=${NONE}
	GatherStatistics=""
	LogRotate=3
	ReportUpdtLapse=${NONE}
	ImportAuto=""
	ReloadConfigAuto=""
	unset $(set | awk -F '=' \
		'$1 ~ "^hotspot[[:digit:]]*_" {print $1}') 2> /dev/null || :

	UpdateReport="y"
	Msgs=""
	MsgsInfo=""
	Ssids=""
	Hotspots=${NONE}
	rm -f "${VARS}" "${SCANNED}" "${STATFILE}"
	: > "${STATFILE}"
	: > "${SCANNED}"
	AddMsg "${msg}"
	PreBackupRotate

	[ -f "/etc/config/${NAME}" ] || \
		: > "/etc/config/${NAME}"

	if ! uci -q show "${NAME}" > /dev/null; then
		msg="err: Invalid config file"
		AddMsg "${msg}"
		_applog "${msg}"
		exit ${ERR}
	fi

	Debug="$(_indicator "$(uci -q get "${NAME}.globals.Debug")" \
		"xtrace")" || :
	! printf '%s\n' "${@}" | grep -qsxiF 'debug' || \
		Debug="y"
	! printf '%s\n' "${@}" | grep -qsxiF 'xtrace' || \
		Debug="xtrace"
	set +o xtrace
	if [ "${Debug}" = "xtrace" ]; then
		exec >> "${LOGFILE}.xtrace" 2>&1
		set -o xtrace
	else
		exec >> "${LOGFILE}" 2>&1
	fi

	if [ "${Debug}" = "xtrace" ]; then
		DevNull="${LOGFILE}.xtrace"
	elif [ -n "${Debug}" ]; then
		DevNull="${LOGFILE}"
	else
		DevNull="/dev/null"
	fi

	while read -r option; do
		[ -n "${option}" ] && \
		value="$(uci -q get "${NAME}.globals.${option}")" || \
			continue
		case "${option}" in
		Debug|remark) : ;;
		WanShutdown)
			WanShutdown="$(_indicator "${value}")"
			;;
		APDisabled)
			APDisabled="$(_indicator "${value}")"
			;;
		ScanAuto)
			ScanAuto="$(_indicator "${value}" "allways")"
			;;
		WAnalyzer)
			WAnalyzer="$(_indicator "${value}")"
			;;
		APChannel)
			APChannel="${value}"
			;;
		ReScan)
			ReScan="$(_indicator "${value}")"
			;;
		ReScanOnNetwFail)
			ReScanOnNetwFail="${value}"
			_check_int_val "ReScanOnNetwFail" 1
			;;
		ReScanAuto)
			ReScanAuto="${value}"
			_check_int_val "ReScanAuto" ${NONE}
			;;
		ConnectAuto)
			ConnectAuto="$(_indicator "${value}")"
			;;
		Sleep)
			Sleep="${value}"
			_check_int_val "Sleep" 20
			;;
		SleepInactive)
			SleepInactive="${value}"
			_check_int_val "SleepInactive" $((Sleep*3))
			;;
		SleepDsc)
			SleepDsc="${value}"
			_check_int_val "SleepDsc" $((Sleep*3))
			;;
		SleepScanAuto)
			SleepScanAuto="${value}"
			_check_int_val "SleepScanAuto" $((Sleep*15))
			;;
		BlackList)
			BlackList="${value}"
			_check_int_val "BlackList" 3
			;;
		BlackListExpires)
			BlackListExpires="${value}"
			_check_int_val "BlackListExpires" ${NONE}
			;;
		BlackListNetwork)
			BlackListNetwork="${value}"
			_check_int_val "BlackListNetwork" 3
			;;
		BlackListNetworkMaxWarn)
			BlackListNetworkMaxWarn="${value}"
			_check_int_val "BlackListNetworkMaxWarn" 3
			;;
		BlackListNetworkExpires)
			BlackListNetworkExpires="${value}"
			_check_int_val "BlackListNetworkExpires" $((10*60))
			;;
		PingWait)
			PingWait="${value}"
			_check_int_val "PingWait" 7
			;;
		MinTrafficBps)
			MinTrafficBps="${value}"
			_check_int_val "MinTrafficBps" 2048
			;;
		ShowTraffic)
			ShowTraffic="$(_indicator "${value}")"
			;;
		DisconnectIdle)
			DisconnectIdle="${value}"
			_check_int_val "DisconnectIdle" ${NONE}
			;;
		GatherStatistics)
			GatherStatistics="$(_indicator "${value}")"
			;;
		LogRotate)
			LogRotate="${value}"
			_check_int_val "LogRotate" 3
			;;
		ReportUpdtLapse)
			ReportUpdtLapse="${value}"
			_check_int_val "ReportUpdtLapse" ${NONE}
			;;
		ImportAuto)
			ImportAuto="$(_indicator "${value}")"
			;;
		ReloadConfigAuto)
			ReloadConfigAuto="$(_indicator "${value}")"
			;;
		*)
			msg="Warn: Invalid config option \"${option}\""
			_applog "${msg}"
			AddMsgInfo "${msg}"
			;;
		esac
	done << EOF
$(uci -q show "${NAME}.globals" | \
sed -nre "/^${NAME}\.globals\.([^=]+)=.*/s//\1/p" | \
sort -u)
EOF

	if [ -n "${Debug}" -o ${#} -gt ${NONE} ]; then
		msg="daemon's command line"
		[ ${#} -gt ${NONE} ] && \
			msg="${msg} options:$(printf ' "%s"' "${@}")" || \
			msg="${msg} is empty"
		AddMsg "${msg}"
		_applog "${msg}"
	fi

	! printf '%s\n' "${@}" | grep -qsxiF 'import' || \
		ImportAuto="y"

	if NtpServers="$(uci -q get "${NAME}.ntp.server")" && \
	[ $(printf '%s' "${NtpServers}" | \
	sed -re \
	"/[[:blank:]]*(('([^']+)')|(\"([^\"]+)\")|([^[:blank:]]+))[[:blank:]]*/ \
	s//\3\5\6\n/g" | \
	wc -l) != 4 ]; then
		NtpServers="${NTPSERVERS}"
		msg="Warn: invalid ntp servers. Replacing by default"
		AddMsgInfo "${msg}"
		_applog "${msg}"
	fi

	while :; do
		hotspot=${Hotspots}
		uci -q show "${NAME}.@hotspot[${hotspot}]" > /dev/null || \
			break
		hotspot_ssid="$(uci -q get "${NAME}.@hotspot[${hotspot}].ssid")" || :
		hotspot_bssid="$(uci -q get "${NAME}.@hotspot[${hotspot}].bssid")" || :
		hotspot_encryption="$(uci -q get "${NAME}.@hotspot[${hotspot}].encryption")" || :
		if [ -z "${hotspot_ssid:=""}" -a -z "${hotspot_bssid:=""}" ] || \
		[ -z "${hotspot_encryption:-}" ]; then
			_msg "Adding hotspot, Invalid config." \
				"No ssid, bssid or encryption specified"
			LogPrio="err"; _log "${msg}"
			AddMsgInfo "err:" "${msg}"
			exit ${ERR}
		fi
		let "Hotspots++,1"
		[ -z "${hotspot_ssid}" ] || \
			eval hotspot${Hotspots}_ssid='${hotspot_ssid}'
		if [ -n "${hotspot_bssid}" ]; then
			bssids="$(BssidsCheck "${hotspot_bssid}")" || {
				_msg "Adding hotspot ${Hotspots}," \
					"Invalid bssid in ${hotspot_bssid}"
				LogPrio="err"; _log "${msg}"
				AddMsgInfo "err:" "${msg}"
				exit ${ERR}
			}
			hotspot_bssid="${bssids}"
			eval hotspot${Hotspots}_bssid='${hotspot_bssid}'
		fi
		eval hotspot${Hotspots}_encryption='${hotspot_encryption}'
		#! dummy="$(uci -q get "${NAME}.@hotspot[${hotspot}].description")" || \
		#	eval hotspot${Hotspots}_description='${dummy}'
		! dummy="$(uci -q get "${NAME}.@hotspot[${hotspot}].key")" || \
			eval hotspot${Hotspots}_key='${dummy}'
		! dummy="$(uci -q get "${NAME}.@hotspot[${hotspot}].key1")" || \
			eval hotspot${Hotspots}_key1='${dummy}'
		! dummy="$(uci -q get "${NAME}.@hotspot[${hotspot}].key2")" || \
			eval hotspot${Hotspots}_key2='${dummy}'
		! dummy="$(uci -q get "${NAME}.@hotspot[${hotspot}].key3")" || \
			eval hotspot${Hotspots}_key3='${dummy}'
		! dummy="$(uci -q get "${NAME}.@hotspot[${hotspot}].key4")" || \
			eval hotspot${Hotspots}_key4='${dummy}'
		! dummy="$(uci -q get "${NAME}.@hotspot[${hotspot}].hidden")" || \
			eval hotspot${Hotspots}_hidden='${dummy}'
		if dummy="$(uci -q get "${NAME}.@hotspot[${hotspot}].blacklisted")"; then
			if [ "${dummy}" != "y" ] && \
			bssids="$(BssidsCheck "${dummy}")"; then
				eval hotspot${Hotspots}_blacklisted='${bssids}'
				for bssid in ${bssids}; do
					_list_append "hotspot${Hotspots}_blacklist" \
						"0.config.0"
				done
			else
				if [ "${dummy}" != "y" ]; then
					_msg "Adding hotspot ${Hotspots}," \
						"Invalid blacklist bssids in ${dummy}" \
						"Blacklisting this hotspot"
					LogPrio="err"; _log "${msg}"
					AddMsgInfo "err:" "${msg}"
				fi
				eval hotspot${Hotspots}_blacklisted='y'
				eval hotspot${Hotspots}_blacklist='0.config.0'
			fi
		fi
		! dummy="$(uci -q get "${NAME}.@hotspot[${hotspot}].check")" || \
			eval hotspot${Hotspots}_check='${dummy}'
		for bssid in ${hotspot_bssid:-""}; do
			_list_append "Ssids" "${Hotspots}${TAB}${bssid}${TAB}${hotspot_ssid}" "?"
		done
		if [ -n "${Debug}" ]; then
			local msg="Adding new hotspot $( \
				HotspotName "${Hotspots}" \
				"${hotspot_bssid:-"${BEL}"}" \
				"${hotspot_ssid:-"${BEL}"}")"
			AddMsgInfo "${msg}"
			_applog "${msg}"
		fi
	done

	BackupRotate

	IfaceWan="$(uci -q get network.wan.ifname)" || :

	WwanNetData || {
		LogPrio="err"; InsertStatMsg "Invalid AP+STA configuration"
		exit ${ERR}
	}
	_applog "Radio device is ${WRadioDevice}"
	_applog "STA network interface is ${WIface}"
	WIfaceCarrier="$(WifiCarrier)"
	[ -n "${WIfaceAP}" ] || \
		_applog "Non standard STA only configuration"
	_applog "WAN interface is ${IfaceWan:-"not available"}"
	SetWanShutdown

	Channels="$(iwinfo "${WRadioDevice}" freqlist | \
		awk '{channel=gensub("[^[:digit:]]", "", "g", $NF)
			if (channel) {printf "%s%d", sep, channel
				sep=" "}
			}')"
	if [ ${Hotspots} -eq ${NONE} ]; then
		ImportAuto="y"
		_msg "No configured hotspots." \
			"Will import the configuration of a connected hotspot."
		InsertStatMsg  "${msg}"
	fi
	d="$(printf '%s' "${Ssids}" | \
		awk 'BEGIN{RS="?"; FS="\t"} {print $2, $3}' | \
		sort | uniq -d)"
	if [ -n "${d}" ]; then
		_msg "Invalid configuration." \
			"Duplicate hotspots SSIDs or BSSIDs"
		LogPrio="err"; InsertStatMsg  "${msg}"
		exit ${ERR}
	fi
	d="$(printf '%s' "${Ssids}" | \
		awk 'BEGIN{RS="?"; FS="\t"} $2 {print $2}' | \
		sort | uniq -d)"
	if [ -n "${d}" ]; then
		_msg "Invalid configuration." \
			"Duplicate hotspots BSSIDs"
		LogPrio="err"; InsertStatMsg  "${msg}"
		exit ${ERR}
	fi

	HotspotsOrder="$(echo $(
		{ printf '%s' "${Ssids}" | \
			awk 'BEGIN{RS="?"; FS="\t"} $2 {print $1}'; 
		printf '%s' "${Ssids}" | \
			awk 'BEGIN{RS="?"; FS="\t"} ! $2 {print $1}'
		} | \
		uniq )
		)"

	Ssids=""
	local hh=1
	for hotspot in ${HotspotsOrder}; do
		eval hotspot_ssid=\"\${hotspot${hotspot}_ssid:-}\"
		eval hotspot_bssid=\"\${hotspot${hotspot}_bssid:-}\"
		for bssid in ${hotspot_bssid:-""}; do
			_list_append "Ssids" "${hotspot}${TAB}${bssid}${TAB}${hotspot_ssid}" "?"
		done

		eval hotspot_hidden=\"\${hotspot${hotspot}_hidden:-}\"
		[ -n "${hotspot_bssid}" -o -z "${hotspot_hidden}" ] || \
			let "hh--" || {
				msg="Warn: Several hotspots specify hidden SSID and not BSSID"
				_applog "${msg}"
				AddMsgInfo "${msg}"
			}
	done

	TryConnection=${NONE}
	ScanErr=""
	WwanErr=${NONE}
	[ -n "${ScanAuto}" ] && \
		ScanRequest=${Hotspots} || \
		ScanRequest=${NONE}
	Hotspot=${NONE}
	WwanSsid=""
	WwanBssid=""
	ConnAttempts=1
	WarnBlackList=""
	BlackListEnds=""
	IndReScan=""
	ReScanAutoTimeout=${NONE}
	RequestTimeout=""
	NetwFailures=${NONE}
	NetwFailTime=${NONE}
	Status=${NONE}
	ConnectedName=""

	/etc/init.d/${NAME}-autoconnect stop >> "${DevNull}" 2>&1
	if [ -n "${GatherStatistics}" ]; then
		printf "%s='%s'\n" \
			"WIface" "${WIface}" \
			"WIfaceSTA" "${WIfaceSTA}" \
			"Ssids" "${Ssids}" \
			"GatherStatistics" "${GatherStatistics}" \
			"TRAFFIFO" "${TRAFFIFO}" > "${VARS}"
		if [ ! -e "${TRAFFIFO}" ]; then
			mkdir -p "$(dirname "${TRAFFIFO}")"
			mkfifo "${TRAFFIFO}"
		fi

		mkdir -p "${GatherStatistics}"
		_applog "Setting up gather traffic statistics"
		/etc/init.d/${NAME}-traffic restart >> "${DevNull}" 2>&1
	else
		/etc/init.d/${NAME}-traffic stop >> "${DevNull}" 2>&1
	fi
	[ -z "${Debug}" ] || \
		_applog "$(StatusName)"
	msg="Configuration reloaded"
	_applog "${msg}"
	AddMsg "${msg}"
	NoSleep="y"
	ConfigModTime=$(_UTCseconds -r "/etc/config/${NAME}")
	ReloadConfig=""
}

ReportHeader() {
	printf '%s\n\n' "${NAME} status report."
	printf '%s\n' "${Msgs}" \
		"Info:"
	if [ -n "${MsgsInfo}" -o ${NetwFailures} -ne ${NONE} ]; then
		[ -z "${MsgsInfo}" ] || \
			printf '%s\n' "${MsgsInfo}"
		[ ${Status} -ne ${CONNECTED} -o ${NetwFailures} -eq ${NONE} ] || \
		printf '%s\n' "${MsgsInfo}" | grep -qsF 'networking failure' || {
			MsgNetwFailures
			printf '%s\n' "$(_datetime) ${msg}"
		}
	fi
	printf '\n'
}

ListScan() {
	local signal seen ciph pair auth bssid channel freq associated dummy ssid
	{ iwinfo | \
	awk 'BEGIN{OFS="\t"}
		/ESSID:/ { ssid=gensub(/.*ESSID:[[:blank:]]+"|"$/, "", "g") }
		/Access Point:/ { bssid=$NF }
		/Mode: .* Channel:/ { mode=$2
			channel=$4
			freq=gensub(/[^[:digit:]]/, "", "g", $5) }
		/PHY name:/{ dev=$NF
			print dev, 0, 0, 0, 0, \
				bssid, channel, freq, mode, "dummy", ssid }'
	cat "${SCANNED}"
	} | \
	while IFS="${TAB}" \
	read -r signal seen ciph pair auth bssid channel freq associated dummy ssid && \
	[ -n "${bssid}" ]; do
		[ -n "${ssid}" -a "${ssid}" != "${HIDDENSSID}" ] && \
			ssid="\"$(printf "${ssid}")\"" || \
			ssid="${NULLSSID}"
		printf '%s\t' \
			"${channel}" \
			"$(printf "${signal}" | sed -re '/^[[:digit:]]+$/s//-&/')" \
			"${freq}" "${associated/-/}" \
			"${bssid}" "${ssid}"
		echo
	done
}

Report() {
	local hotspot m v o
	[ -z "${Debug}" ] || \
		_applog "Writing status report"
	rm -f "${STATFILE}"
	exec > "${STATFILE}"
	ReportHeader
	printf '%s\n' "Radio device is ${WRadioDevice}" \
		"WAN interface is ${IfaceWan:-"not available"}" \
		"STA network interface is ${WIface}" \
		"Detected STA config in wifi-iface ${WIfaceSTA}" \
		"STA connected to network interface ${NIfaceSTA}"
	if [ -n "${WIfaceAP}" ]; then
		printf '%s\n' "Detected AP config in wifi-iface ${WIfaceAP}" \
			"AP connected to network interface ${NIfaceAP}"
	else
		printf '%s\n' "Non standard STA only configuration"
	fi
	printf '\n'
	printf '%s\n' "Autodetected:"
	printf '%s="%s"\n' \
		"WRadioDevice" "${WRadioDevice}" \
		"IfaceWan" "${IfaceWan}" \
		"WIface" "${WIface}" \
		"WIfaceSTA" "${WIfaceSTA}" \
		"NIfaceSTA" "${NIfaceSTA}" \
		"WIfaceAP" "${WIfaceAP}" \
		"NIfaceAP" "${NIfaceAP}"
	[ -z "${Channels}" ] || \
		printf '%s="%s"\n' \
			"Channels" "${Channels}"
	printf '\n%s\n' "Configuration:"
	printf '%s="%s" %s\n' \
		"Debug" "${Debug}" \
			"$(test -z "${Debug}" && echo "# Disabled" || echo "# Enabled")" \
		"WAnalyzer" "${WAnalyzer}" \
			"$(test -z "${WAnalyzer}" && echo "# Disabled" || echo "# Enabled")" \
		"APChannel" "${APChannel}" "# wireless channel" \
		"WanShutdown" "${WanShutdown}" \
			"$(test -z "${WanShutdown}" && echo "# Wan IF enabled" || \
			echo "# Wan IF disabled")" \
		"APDisabled" "${APDisabled}" \
			"$(test -z "${APDisabled}" && echo "# Enabled" || echo "# Disabled")" \
		"ScanAuto" "${ScanAuto}" \
			"$(test -z "${ScanAuto}" && echo "# Disabled" || echo "# Enabled")" \
		"ReScan" "${ReScan}" \
			"$(test -z "${ReScan}" && echo "# Disabled" || echo "# Enabled")" \
		"ReScanOnNetwFail" "${ReScanOnNetwFail}" \
			"$(test ${ReScanOnNetwFail} -eq ${NONE} && echo "# Disabled" || \
			echo "# networking failures")" \
		"ReScanAuto" "${ReScanAuto}" \
			"$(test ${ReScanAuto} -eq ${NONE} && echo "# Disabled" || \
			echo "# seconds")" \
		"ConnectAuto" "${ConnectAuto}" \
			"$(test -z "${ConnectAuto}" && echo "# Disabled" || echo "# Enabled")" \
		"NtpServers" "${NtpServers}" \
			"$(test -n "${NtpServers}" && echo "" || echo "# Disabled")" \
		"ShowTraffic" "${ShowTraffic}" \
			"$(test -z "${ShowTraffic}" && echo "# Disabled" || echo "# Enabled")" \
		"GatherStatistics" "${GatherStatistics}" \
			"$(test -z "${GatherStatistics}" && echo "# Disabled" || echo "# Enabled")" \
		"ImportAuto" "${ImportAuto}" \
			"$(test -z "${ImportAuto}" && echo "# Disabled" || echo "# Enabled")" \
		"ReloadConfigAuto" "${ReloadConfigAuto}" \
			"$(test -z "${ReloadConfigAuto}" && echo "# Disabled" || echo "# Enabled")" \
		"ExitPoints" "${ExitPoints}" \
			"$(test -z "${ExitPoints}" && echo "# Disabled" || echo "# Enabled")" \
		"Sleep" "${Sleep}" "# seconds" \
		"SleepInactive" "${SleepInactive}" "# seconds" \
		"SleepDsc" "${SleepDsc}" "# seconds" \
		"SleepScanAuto" "${SleepScanAuto}" "# seconds" \
		"BlackList" "${BlackList}" \
			"$(test ${BlackList} -eq ${NONE} && echo "# Disabled" || \
			echo "# errors")" \
		"BlackListExpires" "${BlackListExpires}" \
			"$(test ${BlackListExpires} -eq ${NONE} && echo "# Never" || \
			echo "# seconds")" \
		"BlackListNetwork" "${BlackListNetwork}" \
			"$(test ${BlackListNetwork} -eq ${NONE} && echo "# Disabled" || \
			echo "# errors")" \
		"BlackListNetworkMaxWarn" "${BlackListNetworkMaxWarn}" \
			"$(test ${BlackListNetworkMaxWarn} -eq ${NONE} && \
			echo "# Disabled" || echo "# warnings")" \
		"BlackListNetworkExpires" "${BlackListNetworkExpires}" \
			"$(test ${BlackListNetworkExpires} -eq ${NONE} \
			&& echo "# Never" || echo "# seconds")" \
		"PingWait" "${PingWait}" "# seconds" \
		"MinTrafficBps" "${MinTrafficBps}" \
			"$(test ${MinTrafficBps} -eq ${NONE} && echo "# Disabled" || \
			echo "# bytes per second")" \
		"DisconnectIdle" "${DisconnectIdle}" \
			"$(test ${DisconnectIdle} -eq ${NONE} && echo "# Disabled" || \
			echo "# seconds")" \
		"ReportUpdtLapse" "${ReportUpdtLapse}" \
			"$(test ${ReportUpdtLapse} -eq ${NONE} && echo "# Disabled" || \
			echo "# seconds")" \
		"LogRotate" "${LogRotate}" "# log files to keep"

	for hotspot in $(seq ${Hotspots}); do
		echo
		set | \
		grep -se "^hotspot${hotspot}_" | \
		while IFS="=" read -r m v; do
			case "${m}" in
			*_blacklist)
				printf '%s=%s' "${m}" "'"
				o="y"
				_unquote "${v}" | tr -s '\t' '\n' | \
				while IFS="." read -r v r d; do
					[ -n "${v}" ] || \
						continue
					[ -n "${o}" ] && \
						o="" || \
						printf ', '
					printf '%d(%s).%s.%d%s' \
						"${v}" "$([ ${v} -eq ${NONE} ] && \
							echo "Never" || \
							_datetime "--date=@${v}")" \
						"${r}" \
						"${d}" "$([ ${d} -eq ${NONE} ] || \
							echo "($(_datetime "--date=@${d}"))")"
				done
				echo "'"
				;;
			*_blacklisted|*_bssid)
				v="$(_unquote "${v}")"
				echo "${m}='${v%${TAB}}'"
				;;
			*)
				echo "${m}=${v}"
				;;
			esac
		done
	done
	printf '\n%s\n'  "Wireless devices:"
	iwinfo || :
	printf '%s %s\n' "Current hotspot client is" \
		"$(HotspotName)"
	printf '%s %s%s\n' "Hotspot client is" \
		"$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].disabled | \
		grep -qsxF "${UCIDISABLED}" && \
		echo "dis" || echo "en")" "abled"
	printf '%s%s %s\n\n' "Hotspot Wifi connection is" \
		"$(test -n "$(WwanIfaceIP)" || echo " not")" "active"
	if [ -n "${IfaceWan}" ]; then
		printf '%s%s%s\n\n' "WAN interface is " \
			"$(IsWanConnected && echo "connected" || \
			if ! ifconfig "${IfaceWan}" | grep -qswF 'RUNNING'; then
				echo "disabled/down"
			else
				echo "disconnected"
			fi)"
	else
		printf '%s\n\n' "There is no WAN interface"
	fi
	printf '%s\n' "Active default routes: $(ActiveDefaultRoutes)"
	[ $(ActiveDefaultRoutes) -le 1 ] || \
		printf '%s\n' \
			"warn: Multiple default routes, may be must disconnect hotspot $(HotspotName)"
	echo
	ip -4 route show
	if [ -n "${WAnalyzer}" ]; then
		printf '\n%s\n' "Simple WIFI analysis"
		[ -s "${SCANNED}" ] || \
			DoScan "ListScan" || :
		printf '%s %s\n' "Wireless channels in use" \
			"$(_datetime -r "${SCANNED}")"
		printf '%s\t' \
			"Channel" "Signal" "Freq" "A" "AP_BSSID_________" "SSID"
		echo
		ListScan | \
			sort -n -k 1,2
		[ ${Status} -eq ${CONNECTING} ] || \
			ChannelCheck
	fi
	[ -z "${Debug}" ] || \
		_applog "End of status report"
}

CurrentHotspot() {
	Hotspot="$(printf '%s' "${Ssids}" | \
		awk -v ssid="${WwanSsid}" \
		-v bssid="${WwanBssid}" \
		'BEGIN{RS="?"; FS="\t"}
		( $2 == bssid && ( $3 == ssid || ! $3 ) ) || \
		( ! $2 && $3 == ssid ) {n=$1; exit}
		END{print n+0}')"
}

MustScan() {
	[ ${Status} -ne ${CONNECTED} -a ${Status} -ne ${CONNECTING} ] || \
		return ${ERR}
	[ ${ScanRequest} -le ${NONE} -a "${ScanAuto}" != "allways" ] || \
		return ${OK}
	[ -n "${ScanAuto}" ] && [ $(ActiveDefaultRoutes) -eq ${NONE} ]
}

Scanning() {
	local err i=5
	while let "i--"; do
		sleep 1
		! err="$(iw dev "${WIface}" scan passive 3>&2 2>&1 1>&3 3>&-)" 2>&1 || \
			return ${OK}
		[ -z "${Debug}" ] || \
			_applog "${err}"
		if [ ${i} -ne 2 ]; then
			_sleep 5
			continue
		fi
		printf '%s\n' "${err}" | \
		grep -qsF 'command failed: Network is down' || \
			LogPrio="err"
		_log "Can't scan wifi, restarting the network"
		/etc/init.d/network reload
		SetWanShutdown
		WatchWifi
	done
	LogPrio="err"; msg="Serious error: Can't scan wifi for access points"
	[ -z "${Debug}" ] && \
		_applog "${msg}" || \
		_log "${msg}"
	ScanErr="y"
	return ${ERR}
}

KnownHotspot() {
	# global ssid bssid
	# returns ssid1 bssid1
	local hotspots="${@}" \
		bssids hotspot_ssid hidden
	for hotspot in ${hotspots:-${HotspotsOrder}}; do
		eval ssid1=\"\${hotspot${hotspot}_ssid:-}\"
		eval bssids=\"\${hotspot${hotspot}_bssid:-}\"
		eval hidden=\"\${hotspot${hotspot}_hidden:-}\"
		if [ -n "${hidden}" ]; then
			[ "${hidden}" = "y" ] && \
				hotspot_ssid="${HIDDENSSID}" || \
				hotspot_ssid="${hidden}"
		else
			hotspot_ssid="${ssid1}"
		fi
		[ -z "${hotspots}" -o -n "${bssids}" ] || \
			bssids="${WwanBssid}"
		if ( [ -z "${hotspot_ssid}" ] && \
		printf '%s\n' "${bssids:-${NULLBSSID}}" | grep -qswF "${bssid}" ) || \
		[ -n "${hotspot_ssid}" -a "${hotspot_ssid}" = "${ssid}" -a -z "${bssids}" ] || \
		( [ -n "${hotspot_ssid}" -a -n "${bssids}" \
		-a "${hotspot_ssid}" = "${ssid}" ] && \
		printf '%s\n' "${bssids:-${NULLBSSID}}" | grep -qswF "${bssid}" ); then
			bssid1="${bssid}"
			return ${OK}
		fi
		[ -z "${Debug}" ] || \
			if [ -n "${hotspot_ssid}" -a -n "${bssids}" \
			-a "${hotspot_ssid}" = "${ssid}" ] && \
			! printf '%s\n' "${bssids}" | grep -qswF "${bssid}"; then
				_applog "Not selecting SSID&BSSID" \
					"$(HotspotName "${hotspot}" "${bssid}" "${ssid}")"
			fi
	done
	return ${ERR}
}

# returns hotspot ssid bssid channel
DoScan() {
	local forceScan="${1:-}" \
		checkHotspotAvail="${2:-}" \
		discardAssociated="${3:-}" \
		msg

	BlackListExpired

	[ -n "${forceScan}" ] || \
		if ! MustScan; then
			[ -z "${Debug}" ] || \
				_applog "Must not scan"
			return ${ERR}
		fi

	[ -z "${Debug}" ] || \
		_applog "Do-Scan - scanning." \
			"Looking for a$(
			[ -z "${discardAssociated}" ] || echo "nother")" \
			"hotspot"

	rm -f "${SCANNED}"
	Scanning | \
		awk \
		'function trim(s) {
			if (!s) s=$0
			return gensub(/^[[:blank:]]+|[[:blank:]]+$/, "", "g", s)
		}
		function nospaces() {
			return gensub(/[[:blank:]]+/, ",", "g", trim())
		}
		function prt() {
			if (! bssid) return
			rc=-1
			print signal, seen, ciph, pair, auth, \
				bssid, channel, freq, associated, ssid
			bssid=""
		}
		BEGIN{OFS="\t"}
		$1 == "BSS" {
			prt()
			associated="-"
			if ($NF == "associated") associated="y"
			bssid=toupper(substr($2,1,17))
			seen="999999999"
			signal="99"
			ssid=""
			ciph="-"
			pair="-"
			auth="-"
			channel="-"
			freq="-"
			next}
		{if (! bssid) next}
		$1 == "signal:" {
			signal=0-$2
			next}
		/^[[:blank:]]+last seen:[[:blank:]]+/ {
			seen=$3
			next}
		$1 == "SSID:" {
			$1=$1
			ssid=trim()
			next}
		/^[[:blank:]]+\* Group cipher: / {
			$1=$2=$3=""
			ciph=nospaces()
			next}
		/^[[:blank:]]+\* Pairwise ciphers: / {
			$1=$2=$3=""
			pair=nospaces()
			next}
		/^[[:blank:]]+\* Authentication suites: / {
			$1=$2=$3=""
			auth=nospaces()
			next}
		/^[[:blank:]]+\* primary channel: / {
			channel=$4
			next}
		/^[[:blank:]]+freq: / {
			freq=$2
			next}
		END{prt()
		exit rc+1}' | \
		sort -n -k 1,2 > "${SCANNED}" || \
			return ${ERR}

	[ "${forceScan}" != "ListScan" ] || \
		return ${OK}

	if [ -n "${ScanErr}" ]; then
		msg="Wifi scan for access points has been successful"
		[ -z "${Debug}" ] && \
			_applog "${msg}" || \
			_log "${msg}"
		ScanErr=""
	fi

	local ssid1 bssid1 blacklisted \
		signal seen ciph pair auth freq associated dummy \
		warning encryption rc

	warning=""; rc=${ERR}
	while IFS="${TAB}" \
	read -r signal seen ciph pair auth bssid channel freq associated dummy ssid && \
	[ -n "${bssid}" ]; do
		[ -z "${discardAssociated}" -o -z "${associated/-/}" ] || \
			continue
		[ -n "${ssid}" -a "${ssid}" != "${HIDDENSSID}" ] && \
			ssid="$(printf "${ssid}")" || \
			ssid=""
		KnownHotspot ${checkHotspotAvail} || \
			continue
		[ -z "${checkHotspotAvail}" ] || \
			return ${OK}
		eval blacklisted=\"\${hotspot${hotspot}_blacklisted:-}\"
		if [ "${blacklisted}" = "y" ] || \
		printf '%s\n' "${blacklisted:-${NULLBSSID}}" | grep -qswF "${bssid}"; then
			warning="${warning:+"${warning}${LF}"}$( \
				HotspotName "${hotspot}" "${bssid}" "${ssid}")"
			if [ ${hotspot} -eq ${Hotspot} ]; then
				[ -z "${Debug}" ] || \
					_applog "Do-Scan: current hotspot" \
						"$(HotspotName "" "${bssid}")" \
						"is blacklisted"
				rc=2
			fi
			[ \( ${Status} -eq ${DISABLED} -o -z "${WIfaceAP}" -o -n "${APDisabled}" \) \
			-a -z "${Debug}" -a -z "${MsgsChgd}" ] || {
				_msg "Do-Scan: Not selecting blacklisted hotspot" \
					"$(HotspotName "${hotspot}" "${bssid}" \
						"${ssid1:-"${ssid:-"${BEL}"}"}")"
				_applog "${msg}"
				[ -z "${ExitPoints}" ] || \
					((OnConnectBlacklist "${msg}" >> "${DevNull}" 2>&1) &)
			}
			continue
		fi
		if [ "${ssid}" = "${HIDDENSSID}" -o -z "${ssid}" ]; then
			ssid="${BEL}"
		elif [ -z "${ssid1}" ]; then
			ssid1="${ssid}"
		fi
		if [ -z "${Debug}" ]; then
			_applog "Do-Scan selects" \
				"$(HotspotName "${hotspot}" "${bssid}" "${ssid}")"
		else
			_applog "Do-Scan: signal -${signal} dBm ${auth} channel=${channel}" \
				"$(HotspotName "${hotspot}" "${bssid}" "${ssid}")"
			eval encryption=\"\${hotspot${hotspot}_encryption:-}\"
			_applog "info: Do-Scan" \
				"setting encryption mode '${encryption}'," \
				"valid encryption modes '${auth}'"
		fi
		ssid="${ssid1}"
		WarnBlackList=""
		return ${OK}
	done < "${SCANNED}"
	[ -z "${checkHotspotAvail}" ] || \
		return ${ERR}
	if [ -n "${warning}" ]; then
		local pl="$(test $(printf '%s\n' "${warning}" | wc -l) -le 1 || \
			echo "s")"
		warning="$(echo $(printf '%s\n' "${warning}" | sort -n -k 1,1))"
		if [ -n "${Debug}" -o -n "${MsgsChgd}" ] || \
		[ "${WarnBlackList}" != "${warning}" ]; then
			_msg "Warn:" "all available hotspots" \
				"(no${pl}. ${warning}) are blacklisted"
			_applog "${msg}"
			AddMsgInfo "${msg}"
			[ -z "${ExitPoints}" ] || \
				((OnConnectBlacklist "${msg}" >> "${DevNull}" 2>&1) &)
		fi
	else
		[ -z "${Debug}" ] || \
			_applog "Do-Scan: There are no" \
				"${discardAssociated:+"other "}Hotspots available"
	fi
	WarnBlackList="${warning}"
	return ${rc}
}

HotspotAvailable() {
	local hotspot bssid ssid channel
	DoScan "y" "${Hotspot}"
}

ExecConnectAuto() {
	local cmd="${1:-"stop"}" \
		wNetwork

	case "${cmd}" in
	start)
		if wNetwork="$(WwanNetworkIP)"; then
			if ! /etc/init.d/${NAME}-autoconnect mode 2>&1 | \
			grep -qswF "${wNetwork}"; then
				_applog "setup detection of" \
					"no route for destination network \"${wNetwork}\""
				/etc/init.d/${NAME}-autoconnect restart WNetwork="${wNetwork}" >> \
					"${DevNull}" 2>&1
			fi
		elif /etc/init.d/${NAME}-autoconnect running >> "${DevNull}" 2>&1; then
			_applog "stopping automatic connection"
			/etc/init.d/${NAME}-autoconnect stop >> "${DevNull}" 2>&1
		fi
		;;
	stop)
		if [ $(ActiveDefaultRoutes) -le \
		"${NONE}$([ -z "$(WwanIfaceIP)" ] || echo 1)" ]; then
			if ! /etc/init.d/${NAME}-autoconnect mode 2>&1 | \
			grep -qswF 'local'; then
				_applog "setting up automatic connection"
				/etc/init.d/${NAME}-autoconnect restart >> "${DevNull}" 2>&1
			fi
		else
			_applog "Warn: multiple default routes are active." \
				"Can't enable automatic connection"
			if /etc/init.d/${NAME}-autoconnect running >> "${DevNull}" 2>&1; then
				_applog "stopping automatic connection"
				/etc/init.d/${NAME}-autoconnect stop >> "${DevNull}" 2>&1
			fi
		fi
		;;
	esac
}

WwanReset() {
	local disable="${1:-${UCIDISABLED}}" \
		iface="${2:-${WIfaceSTA}}" \
		disabled hotspot ssid bssid channel \
		msg

	channel=""
	if [ \( -z "${WIfaceAP}" -o -n "${APDisabled}" \) \
	-a "${iface}" = "${WIfaceSTA}" ] && \
	[ ${disable} -eq "${UCIDISABLED}" ]; then
		hotspot=${NONE}
		ssid=""
		bssid="${NULLBSSID}"
		channel="auto"

		WwanSsid="${ssid}"
		WwanBssid="${bssid}"
		uci set wireless.@wifi-iface[${iface}].ssid="${ssid}"
		uci set wireless.@wifi-iface[${iface}].bssid="${bssid}"
		uci -q delete wireless.@wifi-iface[${iface}].disabled || :
		msg="Disconnecting current hotspot for the STA interface"
	else
		disabled="$(uci -q get wireless.@wifi-iface[${iface}].disabled | \
			grep -sxF "${UCIDISABLED}")" || :
		[ ${disabled:-${UCIENABLED}} -ne ${disable} ] || \
			return ${OK}
		if [ ${disable} -eq ${UCIDISABLED} ]; then
			uci set wireless.@wifi-iface[${iface}].disabled=${UCIDISABLED}
			[ ${iface} -ne ${WIfaceSTA} ] || \
				[ -z "${WIfaceAP}" -o -n "${APDisabled}" ] || \
					channel="${APChannel}"
		else
			[ "${iface}" != "${WIfaceAP}" ] || \
				channel="${APChannel}"
			uci -q delete wireless.@wifi-iface[${iface}].disabled || :
			StatusHasChanged
		fi
		_msg "$([ ${disable} -eq ${UCIDISABLED} ] && \
			echo "Dis" || echo "En")abling wireless" \
			"$([ "${iface}" = "${WIfaceSTA}" ] && \
				echo "interface to $(HotspotName)" || \
				echo "Access Point")"
	fi

	[ -z "${channel}" ] || \
		ChannelSet "${channel}"

	_log "${msg}"
	AddMsg "${msg}"

	wifi down "${WRadioDevice}"

	wifi up "${WRadioDevice}"
	UpdateReport="y"
}

ChannelsInUse() {
	# global score channel_[0-9]+
	local bssidC \
		signal seen ciph pair auth bssid channel freq associated dummy ssid
	[ -s "${SCANNED}" ] || \
		DoScan "ListScan" || :
	bssidC="$(iwinfo "${WRadioDevice}" info | \
		awk '/Access Point:/ { bssid=$NF }
		/Mode: Client Channel:/ { print bssid; exit }')"
	score=${NONE}
	while IFS="${TAB}" \
	read -r signal seen ciph pair auth bssid channel freq associated dummy ssid && \
	[ -n "${bssid}" ]; do
		[ -n "${associated/-/}" ] || \
		[ "${bssid}" = "${bssidC}" ] || \
		! printf '%s\n' "${Channels}" | grep -qswF "${channel}" || \
			let channel_${channel}+=100-signal,score+=100-signal,1
	done < "${SCANNED}"
}

ChannelCandidates() {
	local cs="${1}" \
		allcheck="${2:-}" \
		c p n points
	for c in ${cs}; do
		if ! printf '%s\n' "${Channels}" | grep -qswF "${c}"; then
			[ -z "${Debug}" ] || \
				_applog "warn: invalid candidate channel ${c}"
			continue
		fi
		! printf '%s' "${candidates}" | \
		awk -v candidate="${c}" \
		'BEGIN{RS="\t"; FS=" "}
		$1 == candidate {rc=-1; exit}
		END{exit rc+1}' || \
			continue

		let p=c-1,n=c+1,1
		let points=2*channel_${c}+channel_${p}+channel_${n},1
		candidates="${candidates}${c} ${points}${TAB}"
		[ -z "${Debug}" ] || \
			_applog "info: candidate channel ${c}, score ${points} points"
		if [ ${points} -lt ${score} ]; then
			let score=points,best=c,1
			[ -n "${allcheck}" ] || \
			[ ${score} -ne ${NONE} ] || \
				return ${OK}
		fi
	done
	return ${ERR}
}

ChannelBest() {
	local channel="${1}" \
		allcheck="${2:-}" \
		c score msg
	best="${channel}"
	if [ -z "${Channels}" ]; then
		best="auto"
	elif [ "${channel}" = "detect" ] || \
	[ "${channel}" != "${channel//[-,${SPACE}]/}" ]; then
		ChannelsInUse
		candidates=""
		best="auto"
		case "${channel}" in
		detect)
			ChannelCandidates "${Channels}" "${allcheck}" || :
			;;
		*)
			for c in ${channel//,/ }; do
				p="${c%-*}"
				n="${c#*-}"
				if [ -n "${p:=$(_min ${Channels})}" -a \
				-n "${n:=$(_max ${Channels})}" ] && \
				[ -z "${p//[0-9]/}" -a -z "${n//[0-9]/}" ] && \
				[ ${p} -le ${n} ]; then
					! ChannelCandidates "$(seq ${p} ${n})" "${allcheck}" || \
					[ -n "${allcheck}" ] || \
						break
				else
					_applog "warn: Invalid channel range \"${c}\", assuming \"auto\""
					best="auto"
					break
				fi
			done
			;;
		esac
		[ -z "${Debug}" ] || \
			_applog "info:" "APChannel=\"${channel}\"," \
				"best \"${best}\"," \
				"candidates \"$(echo ${candidates})\""
	fi
}

ChannelCheck() {
	local best rchannel candidates msg
	rchannel="$(iwinfo "${WRadioDevice}" info | \
		awk '/Mode: .* Channel:/ {print $4; exit}')"
	echo "current channel is \"${rchannel}\""
	[ -n "${rchannel}" -a "${rchannel}" = "${rchannel//[^0-9]}" ] || {
		echo "radio device is in bogus state"
		return ${OK}
	}
	ChannelBest "${rchannel} ${Channels}" \
		"$(test $(printf '%s\n'  "${Channels}" | wc -w) -gt 14 || echo "y")"
	echo "best channel is \"${best}\""
	printf '%s' "Candidate channel/points:"
	printf '%s' "${candidates}" | \
		awk 'BEGIN{RS="\t"; FS=" "}
		{printf "%d %d\n", $2, $1}' | \
		sort -n | \
		awk '{printf " %d/%d", $2, $1}'
	echo
	if [ "${rchannel}" != "${best}" ]; then
		msg="warn: channel conflict, current \"${rchannel}\", best \"${best}\""
		[ -z "${ExitPoints}" ] || \
			((OnNetworkOK "${msg}" >> "${DevNull}" 2>&1) &)
		_applog "${msg}"
		echo "${msg}"
	fi
}

ChannelSet() {
	local channel="${1}" \
		best rchannel candidates
	ChannelBest "${channel}"
	rchannel="$(uci -q get wireless.${WRadioDevice}.channel)" || :
	if [ "${rchannel}" != "${best}" ]; then
		uci set wireless.${WRadioDevice}.channel="${best}"
		[ -z "${Debug}" ] || \
			_applog "info: setting channel to \"${best}\""
	fi
}

HotspotLookup() {
	local hotspot="${1:-}" \
		bssid="${2:-}" \
		ssid="${3:-}" \
		channel="${4:-}"
	# must pass all parms or none

	[ -n "${hotspot}" ] || \
		DoScan || \
		return ${?}

	[ ${Hotspot} -eq ${hotspot} ] || {
		ConnAttempts=1
		Hotspot=${hotspot}
	}

	StatusHasChanged
	ChangeStatus ${CONNECTING}
	local encryption wencryption \
		key key1 key2 key3 key4 \
		wkey wkey1 wkey2 wkey3 wkey4 \
		hidden
	eval encryption=\"\${hotspot${Hotspot}_encryption:-}\"
	eval key=\"\${hotspot${Hotspot}_key:-}\"
	eval key1=\"\${hotspot${Hotspot}_key1:-}\"
	eval key2=\"\${hotspot${Hotspot}_key2:-}\"
	eval key3=\"\${hotspot${Hotspot}_key3:-}\"
	eval key4=\"\${hotspot${Hotspot}_key4:-}\"
	eval hidden=\"\${hotspot${Hotspot}_hidden:-}\"
	wencryption="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].encryption)" || :
	wkey="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].key)" || :
	wkey1="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].key1)" || :
	wkey2="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].key2)" || :
	wkey3="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].key3)" || :
	wkey4="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].key4)" || :
	[ "${APChannel}" != "auto" ] || \
		channel="auto"
	ChannelSet ${channel}
	if [ "${ssid}" != "${WwanSsid}" -a \
	\( -z "${hidden}" -o -n "${ssid}" \) ] || \
	[ "${bssid}" != "${WwanBssid}" -o \
	"${encryption}" != "${wencryption}" -o \
	"${key}" != "${wkey}" -o "${key1}" != "${wkey1}" -o \
	"${key2}" != "${wkey2}" -o "${key3}" != "${wkey3}" -o \
	"${key4}" != "${wkey4}" ]; then
		WwanSsid="${ssid}"
		WwanBssid="${bssid}"
		_log "Hotspot $(HotspotName) found. Applying settings..."
		WwanErr=${NONE}
		uci set wireless.@wifi-iface[${WIfaceSTA}].ssid="${WwanSsid}"
		uci set wireless.@wifi-iface[${WIfaceSTA}].bssid="${WwanBssid}"
		SetEncryption "${encryption}" \
			"${key}" "${key1}" "${key2}" "${key3}" "${key4}"
		if [ -z "${WSTADisabled}" ]; then
			wifi down "${WRadioDevice}"
			wifi up "${WRadioDevice}"
		else
			uci -q delete wireless.@wifi-iface[${WIfaceSTA}].disabled || :
			/etc/init.d/network reload
			SetWanShutdown
		fi
		TryConnection=2
		msg="Connecting to $(HotspotName)..."
		_log "${msg}"
		AddMsg "${msg}"
	elif [ -n "${WSTADisabled}" ]; then
		WwanReset ${UCIENABLED}
		TryConnection=2
	else
		_msg "Client interface to" \
			"$(HotspotName) is already enabled"
		[ -z "${Debug}" -a  -z "${MsgsChgd}" ] || \
			_applog "${msg}"
		[ -z "${MsgsChgd}" ] || \
			AddMsg "${msg}"
	fi
	if [ $((WwanErr++)) -gt ${Hotspots} ]; then
		Interval=${SleepScanAuto}
		ScanRequest=${NONE}
		_msg "Can't connect to any hotspot," \
			"probably configuration is not correct"
		LogPrio="err"; _log "${msg}"
		AddMsg "${msg}"
	else
		Interval=${Sleep}
	fi
	[ ${ScanRequest} -le ${NONE} ] || \
		let "ScanRequest--,1"
}

ReScanning() {
	local discardAssociated="${1:-}" \
		hotspot ssid bssid channel \
		dAssociated msg
	dAssociated="${discardAssociated}"
	if [ -z "${discardAssociated}" -a \
	${NetwFailures} -gt ${NONE} ]; then
		if [ ${BlackListNetworkMaxWarn} -eq ${NONE} -o \
		${BlackListNetworkMaxWarn} -ge ${NetwFailures} ]; then
			msg=""
		else
			MsgNetwFailures
			msg="${msg}, "
		fi
		msg="${msg}looking for another hotspot"
		_applog "${msg}"
		AddMsgInfo "${msg}"
		dAssociated="y"
	fi
	if DoScan "y" "" "${dAssociated}"; then
		if [ -z "${dAssociated}" ] && \
		[ ${hotspot} -eq ${Hotspot} -a "${bssid}" = "${WwanBssid}" ]; then
			msg="Connected hotspot $(HotspotName) gives the most powerful signal"
		else
			StatusHasChanged
			msg="Reconnection required"
			_applog "${msg}"
			AddMsg "${msg}"
			HotspotLookup "${hotspot}" "${bssid}" "${ssid}" "${channel}"
			return ${ERR}
		fi
	else
		msg="Another hotspot is not available"
	fi
	[ ${RequestTimeout:-${NONE}} -lt $(_UTCseconds) ] || \
		UpdateReport="y"
	if [ -z "${discardAssociated}" -o \
	${BlackListNetworkMaxWarn} -eq ${NONE} -o \
	${BlackListNetworkMaxWarn} -ge ${NetwFailures} ]; then
		[ -z "${ExitPoints}" ] || \
			((OnNetworkOK "${msg}" >> "${DevNull}" 2>&1) &)
		_applog "${msg}"
		AddMsgInfo "${msg}"
	fi
}

CheckNetw() {
	{ [ "${Debug}" = "xtrace" ] && \
		exec >&2 || \
		exec > /dev/null 2>&1; } 2> /dev/null
	if [ -n "${CheckInet}" ]; then
		wget -nv --spider -T ${PingWait} --no-check-certificate \
			--bind-address "${CheckInet}" "${CheckAddr}" 2>&1 | \
			grep -sF "200 OK"
	elif [ -n "${CheckSrvr}" ]; then
		printf 'GET %s HTTP/1.0\n\n' "${CheckAddr}" | \
			nc "${CheckSrvr}" ${CheckPort}
	else
		ping -4 -W ${PingWait} -c 3 -I "${WIface}" "${CheckAddr}"
	fi
}

CheckNetworking() {
	local check
	eval check=\"\${hotspot${Hotspot}_check:-}\"
	if [ -z "${check}" ]; then
		[ -n "${ScanAuto}" ] && \
			Interval=${SleepDsc} || \
			Interval=${SleepScanAuto}
		return ${OK}
	fi
	Interval=${Sleep}
	local msg rc
	[ -n "${Gateway}" ] || \
		Gateway="$(WwanGateway)" || \
		Gateway="$(WwanIfaceIP)" || {
			_msg "Serious Error: can't find IP addresses for $(HotspotName)." \
				"Disabling networking check."
			LogPrio="err"; _log "${msg}"
			AddMsg "${msg}"
			UpdateReport="y"
			unset hotspot${Hotspot}_check
			return ${OK}
		}
	[ -n "${CheckAddr}" ] || \
		if CheckSrvr="$(printf '%s\n' "${check}" | \
		sed -nre '\|^http[s]?://([^/]+).*| s||\1|p')" && \
		[ -n "${CheckSrvr}" ]; then
			CheckAddr="${check}"
			if [ -z "$(command -v wget)" -o -z "$(opkg status wget)" ]; then
				[ "${CheckAddr:0:8}" = "https://" ] && \
					CheckPort=443 || \
					CheckPort=80
				[ -z "${Debug}" ] || \
					_applog "check networking, nc ${CheckAddr} ${CheckPort}"
				if [ $(ActiveDefaultRoutes) -gt 1 ]; then
					_msg "Using the nc utility to check networking to URL" \
						"while several default routes are enabled"
					LogPrio="err"; _log "${msg}"
					AddMsg "err:" "${msg}"
				fi
			else
				CheckInet="$(WwanIfaceIP)"
				[ -z "${Debug}" ] || \
					_applog "check networking, wget \"${CheckAddr}\""
			fi
		else
			[ "${check}" = "y" ] && \
				CheckAddr="${Gateway}" || \
				CheckAddr="${check}"
			if [ -z "${CheckAddr}" ]; then
				_msg "Serious Error: no default route for" \
					"$(HotspotName)." \
					"Disabling networking check."
				LogPrio="err"; _log "${msg}"
				AddMsg "${msg}"
				UpdateReport="y"
				unset hotspot${Hotspot}_check
				return ${OK}
			fi
			[ -z "${Debug}" ] || \
				_applog "check networking, ping \"${CheckAddr}\""
		fi
	rc=${ERR}
	if [ ${MinTrafficBps} -gt ${NONE} ]; then
		local r=$(IfaceTraffic) \
			c=$(_UTCseconds) \
			trafficBps
		if [ -n "${CheckTime}" ]; then
			local b=$((r-Traffic)) \
				t=$((c-CheckTime))
			if [ $((t <= 0 ? 0 : b/t)) -ge ${MinTrafficBps} ]; then
				rc=${OK}
				Interval=${Sleep}
				msg="Networking of $(HotspotName) does work"
				let trafficBps=b/t,1
				[ -z "${Debug}" -a -z "${ShowTraffic}" ] || \
					_applog "STA interface transferred" \
					"${b} bytes in ${t} seconds," \
					"${trafficBps} Bps"
				[ ${DisconnectIdle} -eq ${NONE} ] || \
					WorkingTime=${c}
			else
				trafficBps=$((t <= 0 ? 0 : b/t))
				[ -z "${Debug}" ] || \
					_applog "STA interface transferred" \
					"${b} bytes in ${t} seconds" \
					"${trafficBps} Bps"
			fi
		elif [ ${DisconnectIdle} -ne ${NONE} ]; then
			WorkingTime=${c}
		fi
		CheckTime=${c}
		Traffic=${r}
	fi
	if [ ${rc} -ne ${OK} ]; then
		CheckNetw &
		if WaitSubprocess "${!}" "y"; then
			rc=${OK}
			msg="Networking verified on $(HotspotName)"
			Interval=${SleepInactive}
		else
			rc=${?}
			Interval=${Sleep}
		fi
	fi
	if [ ${rc} -eq ${OK} ]; then
		[ -n "${MsgsChgd}" -o -n "${RequestTimeout}" ] || \
			MsgsInfo=""
		if [ ${Status} -eq ${DISCONNECTED} -o ${Status} -eq ${DISABLED} ]; then
			_applog "${msg}"
			AddMsgInfo "${msg}"
		elif [ ${Status} -eq ${CONNECTED} -a ${NetwFailures} -eq ${NONE} ]; then
			[ -z "${Debug}" -a  -z "${MsgsChgd}" ] || \
				_applog "${msg}"
			[ -z "${MsgsChgd}" ] || \
				AddMsgInfo "${msg}"
		else
			_applog "${msg}"
			AddMsgInfo "${msg}"
			NetwFailures=${NONE}
			NetwFailTime=${NONE}
			[ -z "${ExitPoints}" ] || \
				((OnNetworkOK "${msg}" >> "${DevNull}" 2>&1) &)
		fi
		return ${OK}
	elif [ ${rc} -ge 128 -a ${rc} -ne ${RcTERM} ]; then
		return ${OK}
	fi
	[ ${NetwFailures} -ne ${NONE} ] || \
		NetwFailTime=$(_UTCseconds)
	let "NetwFailures++,1"
	if [ ${BlackListNetworkMaxWarn} -eq ${NONE} -o \
	${BlackListNetworkMaxWarn} -ge ${NetwFailures} -o \
	-n "${MsgsChgd}" ]; then
		MsgNetwFailures
		PleaseReport "${msg}"
		[ -z "${ExitPoints}" ] || \
			((OnNetworkFail "${msg}" >> "${DevNull}" 2>&1) &)
		if [ ${BlackListNetworkMaxWarn} -eq ${NetwFailures} ] || \
		[ ${BlackListNetworkMaxWarn} -ne ${NONE} -a \
		${BlackListNetworkMaxWarn} -lt ${NetwFailures} -a \
		-n "${MsgsChgd}" ]; then
			msg="Max. warnings due to networking failures has been reached"
			_applog "${msg}"
			AddMsgInfo "${msg}"
			[ -z "${ExitPoints}" ] || \
				((OnNetworkFailMaxWarn "${msg}" >> "${DevNull}" 2>&1) &)
		fi
	else
		Interval=${SleepInactive}
	fi
	local rs=""
	if [ ${ReScanOnNetwFail} -ne ${NONE} ] && \
	[ ${NetwFailures} -ge ${ReScanOnNetwFail} ]; then
		ReScanning "y" || \
			return ${ERR}
		rs="y"
	fi
	if [ ${BlackListNetwork} -ne ${NONE} ] && \
	[ ${NetwFailures} -ge ${BlackListNetwork} ]; then
		BlackListHotspot "network" "${BlackListNetworkExpires}" "${msg}"
		[ -n "${rs}" ] || \
			ReScanning "y" || \
				return ${ERR}
		WwanReset
		ChangeStatus ${DISCONNECTED}
		[ -n "${ScanAuto}" ] && \
			ScanRequest=${Hotspots} || \
			ScanRequest=${NONE}
		return ${ERR}
	fi
	NoSleep=""
}

ReportOrStat() {
	local tl
	if [ "${WIfaceCarrier}" != "$(WifiCarrier)" ] || \
	[ -n "${UpdateReport}" ]; then
		MsgsChgd=""
		UpdateReport=""
		Report &
		printf '%d\t' "${!}"
		WIfaceCarrier="$(WifiCarrier)"
	elif [ -n "${MsgsChgd}" ]; then
		MsgsChgd=""
		UpdateReport=""
		( {	ReportHeader
			sed -ne '/^Radio device is /,$ {p}' < "${STATFILE}"
		} > "${STATFILE}.part"
		mv -f "${STATFILE}.part" "${STATFILE}" ) &
		printf '%d\t' "${!}"
	elif [ ${ReportUpdtLapse} -ne ${NONE} ] && \
	( [ $((tl=$(_UTCseconds)-MsgTime)) -lt ${NONE} ] || \
	[ ${ReportUpdtLapse} -lt ${tl} ] ); then
		RequestingStatus "Time lapse exceeded, requesting a report update"
	fi
}

CalcLap() {
	local l="${1}"
	let l=l-$(_UTCseconds),1
	[ ${l} -gt ${NONE} ] || \
		return ${ERR}
	let lapse=lapse<=l?lapse:l,1
}

CalcLapse() {
	local lapse=${Interval}
	if [ ${Status} -eq ${DISABLED} \
	-o \( \( -z "${WIfaceAP}" -o -n "${APDisabled}" \) \
	-a ${Status} -eq ${DISCONNECTED} \) ]; then
		[ -z "${BlackListEnds}" ] || \
			CalcLap "${BlackListEnds}" || \
			return ${ERR}
	elif [ ${Status} -eq ${CONNECTED} ]; then
		[ -z "${WorkingTime}" ] || \
			CalcLap "$((WorkingTime+DisconnectIdle))" || \
			return ${ERR}
		[ ${ReScanAuto} -eq ${NONE} ] || \
			CalcLap "${ReScanAutoTimeout}" || \
			return ${ERR}
		[ -z "${BlackListEnds}" ] || \
			CalcLap "${BlackListEnds}" || \
			return ${ERR}
	fi
	printf '%d\n' ${lapse}
}

Settle() {
	local pidSleep="" pidsChildren lapse
	IndReScan=""
	[ -n "${NoSleep}" ] || \
		if lapse="$(CalcLapse)"; then
			[ -z "${Debug}" ] || \
				_applog "sleeping ${lapse} seconds"
			_sleep ${lapse} &
			pidSleep=${!}
		fi
	ReportOrStat > /dev/null
	pidsChildren="$(pgrep -P ${$} | grep -svwF ${pidSleep:-0})" || :
	[ -z "${pidsChildren}" ] || \
		WaitSubprocess "${pidsChildren}" "y" || :
	if [ -n "${pidSleep}" ]; then
		[ -z "${NoSleep}" ] || \
			kill -s TERM ${pidSleep} 2> /dev/null || :
		if ! WaitSubprocess ${pidSleep} \
		"$([ ${Status} -ne ${CONNECTING} ] || \
		echo "${RcISTAT}" ${RequestTimeout:+"${RcISCAN}"})"; then
			WwanErr=${NONE}
			[ -z "${ScanAuto}" ] || \
				ScanRequest=${Hotspots}
		fi
		[ -z "${Debug}" ] || \
			_applog "sleeping ended"
		pidsChildren="$(ReportOrStat)"
		[ -z "${pidsChildren}" ] || \
			WaitSubprocess "${pidsChildren}" "y" || :
	fi
	NoSleep=""
}

WifiStatus() {
	# constants
	readonly NAME APPNAME="${NAME}" LOGFILE="/var/log/${NAME}" \
		STATFILE="/var/log/${NAME}.stat" \
		TRAFFIFO="/var/spool/${NAME}.fifo" \
		VARS="/var/spool/${NAME}-vars.txt" \
		SCANNED="/var/log/${NAME}.scan" \
		IRELOAD="HUP" INETW="ALRM" ISCAN="USR1" ISTAT="USR2" \
		OK=0 ERR=1 UCIENABLED=0 UCIDISABLED=1 \
		LF=$'\n' TAB=$'\t' BEL=$'\x07' SPACE=$' \t\n\r' \
		HIDDENSSID="\x00\x00\x00\x00\x00\x00\x00\x00\x00" \
		NULLSSID="unknown" NULLBSSID="00:00:00:00:00:00" \
		NTPSERVERS="'0.pool.ntp.org' '1.pool.ntp.org' '2.pool.ntp.org' '3.pool.ntp.org'" \
		NONE=0 DISCONNECTED=1 CONNECTING=2 DISABLED=3 CONNECTED=4
	readonly RcIRELOAD="$(_RcInt "${IRELOAD}")" \
		RcINETW="$(_RcInt "${INETW}")" \
		RcISCAN="$(_RcInt "${ISCAN}")" \
		RcISTAT="$(_RcInt "${ISTAT}")" \
		RcTERM="$(_RcInt "TERM")"
	# config variables
	local Debug ScanAuto ReScan ReScanOnNetwFail ReScanAuto ConnectAuto \
		APChannel WAnalyzer APDisabled NtpServers \
		Sleep SleepInactive SleepDsc SleepScanAuto WanShutdown \
		BlackList BlackListExpires \
		BlackListNetwork BlackListNetworkMaxWarn BlackListNetworkExpires \
		PingWait MinTrafficBps ShowTraffic DisconnectIdle OnConnect \
		GatherStatistics LogRotate ReportUpdtLapse ImportAuto ReloadConfigAuto
	# internal variables, daemon scope
	local WwanSsid WwanBssid WSTADisabled WwanErr \
		Ssids Hotspots HotspotsOrder IfaceWan \
		ConnectedName ScanRequest ScanErr IndReScan \
		Status MsgsChgd Msgs MsgsInfo MsgTime ExitPoints="" \
		WIfaceCarrier ReloadConfig ConfigModTime \
		UpdateReport Interval NoSleep BlackListEnds \
		Hotspot ConnAttempts NetwFailures NetwFailTime Traffic CheckTime \
		WorkingTime LogPrio WarnBlackList \
		ReScanAutoTimeout RequestTimeout \
		Gateway CheckAddr CheckSrvr CheckInet CheckPort \
		TryConnection WIface WIfaceAP WIfaceSTA WRadioDevice Channels \
		NIfaceAP NIfaceSTA \
		WwanDisconnected DevNull msg rc

	export APPNAME TRAFFIFO ConnectAuto GatherStatistics Debug LogRotate NtpServers

	trap '_exit' EXIT
	trap 'InsertStatMsg "Stopping"; exit' INT

	LoadConfig "${@}" || {
		LogPrio="err"; InsertStatMsg "Invalid configuration."
		exit ${ERR}
	}
	[ ! -s "/usr/lib/${NAME}/exit-points.sh" ] || {
		. "/usr/lib/${NAME}/exit-points.sh"
		ExitPoints="y"
	}
	Interval=${Sleep}

	trap 'RequestingReload' "${IRELOAD}"
	trap 'RequestingNetwork' "${INETW}"
	trap 'RequestingScan' "${ISCAN}"
	trap 'RequestingStatus' "${ISTAT}"

	while :; do
		Settle
		[ -n "${ReloadConfig}" -o -z "${ReloadConfigAuto}" ] || \
		[ ${ConfigModTime} -eq $(_UTCseconds -r "/etc/config/${NAME}") ] || {
			LogPrio="warn"
			InsertStatMsg "Configuration file has been modified"
			ReloadConfig="y"
		}
		if [ -n "${ReloadConfig}" ]; then
			LoadConfig || {
				LogPrio="err"
				InsertStatMsg "Invalid configuration."
				exit ${ERR}
			}
			continue
		fi
		rc=""
		[ -z "${NIfaceAP}" ] || \
			if [ -n "$(uci -q get network.${NIfaceAP}.disabled)" ]; then
				uci -q delete network.${NIfaceAP}.disabled || :
				LogPrio="warn"
				_log "Enabling network device ${NIfaceAP}"
				rc=1
			fi
		if [ -n "$(uci -q get network.${NIfaceSTA}.disabled)" ]; then
			uci -q delete network.${NIfaceSTA}.disabled || :
			LogPrio="warn"
			_log "Enabling network device ${NIfaceSTA}"
			rc=1
		fi
		[ -z "${rc}" ] || {
			LogPrio="warn"
			_log "restarting the network"
			/etc/init.d/network reload
			Interval=${Sleep}
			UpdateReport="y"
			continue
		}
		WSTADisabled="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].disabled | \
			grep -sxF "${UCIDISABLED}")" || :
		if [ -n "${WIfaceAP}" ]; then
			if [ -n "${APDisabled}" ]; then
				rc=""
				[ "$(uci -q get wireless.@wifi-iface[${WIfaceAP}].disabled)" = ${UCIDISABLED} ] || {
					WwanReset ${UCIDISABLED} "${WIfaceAP}"
					rc=1
				}
				[ -z "${WSTADisabled}" ] || {
					WwanReset ${UCIENABLED}
					rc=1
				}
				[ -z "${rc}" ] || {
					Interval=${Sleep}
					UpdateReport="y"
					continue
				}
			elif [ "$(uci -q get wireless.@wifi-iface[${WIfaceAP}].disabled)" = ${UCIDISABLED} ]; then
					WwanReset ${UCIENABLED} "${WIfaceAP}"
					Interval=${Sleep}
					UpdateReport="y"
					continue
			fi
		elif [ -n "${WSTADisabled}" ]; then # -a -z "${WIfaceAP}"
			WwanReset ${UCIENABLED}
			Interval=${Sleep}
			UpdateReport="y"
			continue
		fi

		[ -z "${RequestTimeout}" ] || \
		[ ${RequestTimeout} -ge $(_UTCseconds) ] || \
			RequestComplete "Scan request timeout"
		WwanSsid="$(uci -q get wireless.@wifi-iface[${WIfaceSTA}].ssid)" || :
		WwanBssid="$(_toupper "$(uci -q get \
			wireless.@wifi-iface[${WIfaceSTA}].bssid)")" || :
		WwanDisconnected="$(test -n "${WSTADisabled}" || IsWwanDisconnected)"
		CurrentHotspot
		if [ -z "${WSTADisabled}" -a -z "${WwanDisconnected}" ]; then
			TryConnection=${NONE}
			ScanErr=""
			WwanErr=${NONE}
			if [ "${ConnectedName}" != "$(HotspotName)" -o \
			${Status} -ne ${CONNECTED} ]; then
				if [ ${Hotspot} -eq ${NONE} ]; then
					LogPrio="warn"
					_log "Connected to a non-configured hotspot:" \
						"$(HotspotName)"
					if [ -n "${ImportAuto}" ]; then
						ImportAuto=""
						if ImportHotspot; then
							_msg "This connected hotspot has been imported" \
								"to the config file"
							_applog "${msg}"
							AddMsg "${msg}"
							NoSleep="y"
							ReloadConfig="y"
							continue
						fi
						_msg "Can't import this connected hotspot" \
							"to the config file." \
							"Disabling ImportAuto"
						UpdateReport="y"
						_applog "${msg}"
						AddMsg "${msg}"
					fi
				fi
				ValidateSsidBssid || \
					continue
				NetwFailures=${NONE}
				NetwFailTime=${NONE}
				Gateway=""; CheckAddr=""; CheckInet=""; CheckTime=""
				WorkingTime=""
				if CheckNetworking; then
					[ -z "${WAnalyzer}" ] || \
						awk -v bssid="${WwanBssid}" \
							'BEGIN{FS="\t"}
							$6 == bssid {rc=-($9 == "y")
							exit}
							END{exit rc+1}' < "${SCANNED}" || \
							DoScan "ListScan" || :
					UpdateReport="y"
					[ \( -n "${WIfaceAP}" -a -z "${APDisabled}" \) \
					-o ${Status} -eq ${CONNECTING} ] || \
						StatusHasChanged
					msg="Connected to $(HotspotName)"
					_log "${msg}"
					AddMsg "${msg}"
					ChangeStatus ${CONNECTED}
					ConnectedName="$(HotspotName)"
					ScanRequest=${NONE}
					RequestComplete
					[ "${ReScanAuto}" -eq ${NONE} ] || \
						let "ReScanAutoTimeout=$(_UTCseconds)+ReScanAuto,1"
				fi
			elif [ -n "${IndReScan}" ] || \
			( [ "${ReScanAuto}" -ne ${NONE} ] && \
			[ ${ReScanAutoTimeout} -le $(_UTCseconds) ] ); then
				[ "${ReScanAuto}" -eq ${NONE} ] || \
					let "ReScanAutoTimeout=$(_UTCseconds)+ReScanAuto,1"
				ReScanning && \
					NoSleep="y" || \
					RequestComplete
			elif CheckNetworking; then
				msg="Connected to $(HotspotName)"
				[ -z "${Debug}" -a  -z "${MsgsChgd}" ] || \
					_applog "${msg}"
				if [ -n "${WorkingTime}" ] && \
				[ $((WorkingTime+DisconnectIdle-$(_UTCseconds))) -le ${NONE} ]; then
					msg="Disconnecting idle hotspot $(HotspotName)"
					_applog "${msg}"
					[ -z "${ExitPoints}" ] || \
						((OnDisconnectIdle "${msg}" >> "${DevNull}" 2>&1) &)
					WwanReset
					ChangeStatus ${DISCONNECTED}
				elif [ $(ActiveDefaultRoutes) -gt 1 ]; then
					msg="Multiple default routes,"
					if [ "${ScanAuto}" = "y" ]; then
						msg="${msg} disconnecting hotspot $(HotspotName)"
						WwanReset
						ChangeStatus ${DISCONNECTED}
					else
						msg="warn: ${msg} may be this hotspot must be disconnected ?"
					fi
					_applog "${msg}"
					[ -z "${ExitPoints}" ] || \
						((OnDisconnectIdle "${msg}" >> "${DevNull}" 2>&1) &)
				else
					[ -z "${MsgsChgd}" ] || \
						ChangeStatus ${CONNECTED}
					RequestComplete
				fi
			fi
			BlackListExpired
			continue
		fi
		if [ ${TryConnection} -gt ${NONE} ]; then
			if HotspotAvailable; then
				let "TryConnection--,1"
				continue
			fi
			TryConnection=${NONE}
			msg="Hotspot $(HotspotName) is gone while connecting"
			_log "${msg}"
			AddMsg "${msg}"
		fi
		if [ -z "${WSTADisabled}" -a -n "${WwanDisconnected}" ]; then
			if [ ${Status} -eq ${CONNECTED} ]; then
				StatusHasChanged
				msg="Lost connection $(HotspotName)"
				_log "${msg}"
				AddMsg "${msg}"
			else
				if [ -n "${WIfaceAP}" -a -z "${APDisabled}" ] && \
				[ ${Status} -eq ${DISCONNECTED} ]; then
					msg="Disabling wireless STA device, Again ?"
					[ -z "${Debug}" -a  -z "${MsgsChgd}" ] || \
						_applog "${msg}"
					[ -z "${MsgsChgd}" ] || \
						AddMsg "${msg}"
				fi
				if [ ${Status} -eq ${CONNECTING} ]; then
					_msg "${ConnAttempts} unsuccessful" \
						"connection$([ ${ConnAttempts} -le 1 ] || echo "s")" \
						"to $(HotspotName)"
					AddMsg "${msg}"
					[ -z "${ExitPoints}" ] || \
						((OnConnectFail "${msg}" >> "${DevNull}" 2>&1) &)
					if [ ${BlackList} -ne ${NONE} ] && \
					[ ${ConnAttempts} -ge ${BlackList} ]; then
						BlackListHotspot "connect" "${BlackListExpires}" \
							"${msg}"
					else
						LogPrio="warn"; _log "${msg}"
						let "ConnAttempts++,1"
					fi
				fi
			fi
			ChangeStatus ${DISCONNECTED}
			[ -n "${ScanAuto}" ] && \
				ScanRequest=${Hotspots} || \
				ScanRequest=${NONE}
			if HotspotLookup; then
				continue
			elif [ ${?} -ne ${ERR} -o \
			\( -n "${WIfaceAP}" -a -z "${APDisabled}" \) ]; then
				WwanReset
			fi
			[ \( -n "${WIfaceAP}" -a -z "${APDisabled}" \) \
			-o ${Status} -ne ${NONE} ] || \
				MsgsChgd="y"
			if [ -n "${WIfaceAP}" -a -z "${APDisabled}" ]; then
				Interval=${Sleep}
				continue
			fi
		elif HotspotLookup; then
			continue
		fi
		WwanErr=${NONE}
		if MustScan; then
			msg="No hotspots available"
		elif [ -n "${ConnectAuto}" ]; then
			msg="Disconnected; to auto-connect start a communication or request an scan"
		else
			msg="Disconnected; to connect request an scan"
		fi
		if [ \( -z "${WIfaceAP}" -o -n "${APDisabled}" \) \
		-a ${Status} -ne ${DISCONNECTED} ]; then
			AddMsg "${msg}"
			ChangeStatus ${DISCONNECTED}
		fi
		if [ ${Status} -ne ${DISABLED} -a \
		\( -n "${WIfaceAP}" -a -z "${APDisabled}" \) ]; then
			_log "${msg}"
			AddMsg "${msg}"
			ChangeStatus ${DISABLED}
		elif [ -n "${MsgsChgd}" ]; then
			_applog "${msg}"
			AddMsgInfo "${msg}"
			[ -z "${ExitPoints}" ] || \
				((OnDisconnect "${msg}" >> "${DevNull}" 2>&1) &)
		fi
		if [ -n "${ScanAuto}" ] && \
		[ -z "${WSTADisabled}" -a \
		\( -n "${WIfaceAP}" -a -z "${APDisabled}" \) ]; then
			Interval=${Sleep}
		elif [ -n "${ScanAuto}" ] && \
		[ $(ActiveDefaultRoutes) -eq ${NONE} ]; then
			Interval=${SleepDsc}
		else
			Interval=${SleepScanAuto}
		fi
		[ ${ScanRequest} -le ${NONE} ] || \
			let "ScanRequest--,1"
		RequestComplete
	done
}

set -o errexit -o nounset -o pipefail +o noglob +o noclobber
NAME="$(basename "${0}")"
case "${1:-}" in
start)
	shift
	WifiStatus "${@}"
	;;
*)
	echo "Wrong arguments" >&2
	exit 1
	;;
esac
